/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.http
 * File:    ChannelRep.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.http;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;

/**
 * Stores objects of class {@link ChannelThread}. Uses a HashMap internally.
 */
public class ChannelRep {

	private ConcurrentMap<ChannelIdentifier,ChannelThread> mChannelThreads;
	
	public ChannelRep(){
		mChannelThreads =  new ConcurrentHashMap<ChannelIdentifier, ChannelThread>();
	}
	
	public void add(ChannelThread ct){
		mChannelThreads.put(ct.getChannelIdentifier(), ct);
	}
	
	public ChannelThread getByChannelId(ChannelIdentifier ci){
		return mChannelThreads.get(ci);
	}
	
	public void remove(ChannelThread ct) {
		mChannelThreads.remove(ct.getChannelIdentifier());
	}
}
