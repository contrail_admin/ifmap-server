/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.http
 * File:    CertificateChannelAuth.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.http;

import java.net.Socket;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.security.cert.X509Certificate;

import org.apache.http.HttpRequest;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.exceptions.ChannelAuthException;

/**
 * Implementation for the certificate-based authentication.
 * 
 * @author tr
 *
 */
public class CertificateChannelAuth extends ChannelAuth {

	private ClientIdentifier mClientId;
	
	public CertificateChannelAuth(Socket socket) {
		super(socket);
	}
	
	@Override
	public void authenticate(HttpRequest request) throws ChannelAuthException
	{	
		// Extract the DN name only on the first call of this method.
		// Further calls are simply nops.
		if (mClientId == null) {
			X509Certificate[] x509;
			String dn = null;
			try {
				x509 = ((SSLSocket) getSocket()).getSession().getPeerCertificateChain();
				dn = x509[0].getSubjectDN().getName();
			} catch (SSLPeerUnverifiedException e) {
				throw new ChannelAuthException("SSL verification failed!");
			}
			mClientId = new ClientIdentifier(getCommonName(dn));
		}
	}


	@Override
	public ClientIdentifier getClientIdentifier() {		
		return mClientId;
	}
	

	private static final String CN_ATTR = "CN=";
	
	/**
	 * From the issuer name, extract the Common name
	 * 
	 * @param dn
	 * @return
	 */
	private String getCommonName(String dn) {
		StringBuffer ret = new StringBuffer();
		
		int cnIdx = dn.indexOf(CN_ATTR);
		
		if (cnIdx > -1) {
			cnIdx += CN_ATTR.length();
			String cn = dn.substring(cnIdx);
			ret.append(cutOffAtFirstComma(cn));
		}
		
		if (ret.length() == 0)
			ret.append("CommonName");
	
		// return result, replace spaces with underscores
		return ret.toString().replace(' ', '_');
	}

	private String cutOffAtFirstComma(String str) {
		int commaIdx = str.indexOf(',');
		if (commaIdx > -1) {
			return str.substring(0, commaIdx);
		}
		return str;
	}
}
