/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.http
 * File:    CertificateChannelAuthFactory.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.http;

import java.net.Socket;

import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Factory class to create instances of class  {@link CertificateChannelAuth},
 * to be used if a MAPC uses certificate-based authentication.
 * 
 * @author aw
 *
 */
public class CertificateChannelAuthFactory implements ChannelAuthFactory {

	@Override
	public ChannelAuth createChannelAuth(Socket socket) {
		NullCheck.check(socket, "socket is null");
		return new CertificateChannelAuth(socket);
	}
}
