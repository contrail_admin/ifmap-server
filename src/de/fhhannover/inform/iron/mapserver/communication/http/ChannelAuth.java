/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.http
 * File:    ChannelAuth.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.http;

import java.net.Socket;

import org.apache.http.HttpRequest;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.exceptions.ChannelAuthException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Defines the authentication-methods used by {@link ChannelThread}
 * 
 * @author tr
 */


public abstract class ChannelAuth {
	
	/**
	 * The socket, this {@link ChannelAuth} instance is used with.
	 * This is needed for the certificate-based authentication.
	 * It's useless for the basic authentication, but we can generalize
	 * basic and certificate-based authentication easier if we introduce it
	 * here.
	 */
	private final Socket mSocket;
	
	protected ChannelAuth(Socket socket) {
		NullCheck.check(socket, "socket is null");
		mSocket = socket;
	}
	
	/**
	 * Returns the client-identifier
	 * @return An unique ID for every client
	 */
	public abstract ClientIdentifier getClientIdentifier();
	
	/**
	 * Performs the authentication process.
	 * 
	 * This method is called for each HTTP request. Whether the authentication
	 * is done each time or only once is up to the implementation.
	 * Basic Authentication, for example, should do the authentication for each
	 * HTTP request.
	 * 
	 * Classes that implement this method must throw a {@link ChannelAuthException}
	 * when an error occurred
	 * 
	 * @param request
	 */
	public abstract void authenticate(HttpRequest request) throws ChannelAuthException;
	
	
	/**
	 * @return the socket used for the channel.
	 */
	public Socket getSocket() {
		return mSocket;
	}
}
