/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    TimerExpiredEvent.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * A {@link Event} implementation which is generated by a {@link SessionTimer}.
 * It occurs when a IF-MAP session has timed out.
 * 
 * @author aw
 *
 */
public class TimerExpiredEvent implements Event {
	
	/**
	 * Indicates which IF-MAP session belongs to this {@link TimerExpiredEvent}.
	 */
	private final String mSessionId;
	
	
	public TimerExpiredEvent(String sessionId) {
		NullCheck.check(sessionId, "sessionId is null");
		
		if (sessionId.length() == 0) {
			throw new RuntimeException("sessionId too short");
		}
		
		mSessionId = sessionId;
	}
	
	public String getSessionId() {
		return mSessionId;
	}

	@Override
	public void dispatch(EventProcessor ep) {
		ep.processTimerExpiredEvent(this);
	}
}
