/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    CloseChannelAction.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.http.ActionProcessor;
import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;

/**
 * A {@link Action} implementation which instructs the {@link ActionProcessor}
 * to close the channel indicated by the included {@link ChannelIdentifier}.
 * <br>
 * Note: This is currently unused as the {@link EventProcessor} does not instruct
 * the {@link ActionProcessor} to close channels at the moment.
 * <br>
 * 
 * @author aw
 */
public class CloseChannelAction extends ChannelAction {

	/**
	 * Construct a {@link CloseChannelAction} object with a given
	 * {@link ChannelIdentifier} object.
	 * 
	 * @param channelIdentifier
	 */
	public CloseChannelAction(ChannelIdentifier channelIdentifier,
			ClientIdentifier clientId) {
		super(channelIdentifier, clientId);
	}
}
