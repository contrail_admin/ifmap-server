/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    ClosedChannelEvent.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;

/**
 * A {@link Event} implementation indicating that a channel was closed.
 * Such an event must be processed accordingly by the {@link EventProcessor}.
 * 
 * @author aw
 */
public class ClosedChannelEvent extends ChannelEvent {
	
	/**
	 * Construct a {@link ClosedChannelEvent} object with a given
	 * {@link ChannelIdentifier} object.
	 * 
	 * @param channelIdentifier
	 */
	public ClosedChannelEvent(ChannelIdentifier channelIdentifier) {
		super(channelIdentifier);
	}

	@Override
	public void dispatch(EventProcessor ep) {
		ep.processClosedChannelEvent(this);
	}
}
