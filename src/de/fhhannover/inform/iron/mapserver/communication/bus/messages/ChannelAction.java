/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    ChannelAction.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Abstract {@link Action} implementation containing a {@link ChannelIdentifier}
 * object to identify the channel this {@link Action} belongs to.
 * 
 * @author aw
 */
public abstract class ChannelAction implements Action {

	/**
	 * Identifies the channel the {@link Action} belongs to.
	 */
	private final ChannelIdentifier mChannelIdentifier;
	private final ClientIdentifier mClientdentifier;
	
	/**
	 * Constructor with simple sanity check. A {@link ChannelIdentifier} is
	 * immutable, so we don't have to do any copies here.
	 * 
	 * @param channelIdentifier
	 */
	public ChannelAction(ChannelIdentifier channelIdentifier,
			ClientIdentifier clientIdentifier) {
		NullCheck.check(channelIdentifier, "channelIdentifier is null");
		NullCheck.check(clientIdentifier, "clientIdentifier is null");
		mChannelIdentifier = channelIdentifier;
		mClientdentifier = clientIdentifier;
	}
	
	public ChannelIdentifier getChannelIdentifier() {
		return mChannelIdentifier;
	}
	
	public ClientIdentifier getClientIdentifier() {
		return mClientdentifier;
	}
}
