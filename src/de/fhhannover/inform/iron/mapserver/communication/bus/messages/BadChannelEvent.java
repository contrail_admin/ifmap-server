/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    BadChannelEvent.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;

/**
 * Simple {@link ChannelEvent} implementation indicating that something
 * went wrong with a channel.
 * 
 * Two cases which may occur are:
 *  - A MAPC issues a request while no response was send on the channel
 *  - Sending of a response fails.
 *  
 * Both cases may be handled such that the MAPC session is closed. Because
 * something went really wrong if this happens.
 * 
 * @author aw
 *
 */
public class BadChannelEvent extends ChannelEvent {

	public BadChannelEvent(ChannelIdentifier channelIdentifier) {
		super(channelIdentifier);
	}

	@Override
	public void dispatch(EventProcessor ep) {
		ep.processBadChannelEvent(this);
	} 
}
