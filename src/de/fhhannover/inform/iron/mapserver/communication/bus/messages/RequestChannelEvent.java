/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    RequestChannelEvent.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import java.io.InputStream;


import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * A {@link Event} implementation representing an incoming request.
 * The channel used for this request is given by the {@link ChannelIdentifier}
 * object. The MAPC is identified by the given {@link ClientIdentifier} object.
 * 
 * @author aw
 */
public class RequestChannelEvent extends ChannelEvent {
	
	/**
	 * Indicates the MAPC responsible for this request.
	 */
	private final ClientIdentifier mClientIdentifier;

	/**
	 * Contains the content of the request, i.e the HTTP body.
	 */
	private final InputStream mRequestContent;
	
	/**
	 * Indicates whether this is the first request on the channel.
	 */
	private final boolean mFirstRequest;

	/**
	 * Constructs a {@link RequestChannelEvent} with the needed parameters.
	 * Does some sanity checks.
	 * 
	 * @param channelIdentifier
	 * @param clientIdentifier
	 * @param requestContent
	 * @param firstRequest true if this is the first request on the channel
	 */
	public RequestChannelEvent(ChannelIdentifier channelIdentifier,
			ClientIdentifier clientIdentifier, InputStream requestContent,
			boolean firstRequest) {
		super(channelIdentifier);
		NullCheck.check(clientIdentifier, "clientIdentifier is null");
		NullCheck.check(requestContent, "requestContent is null");
		mClientIdentifier = clientIdentifier;
		mRequestContent = requestContent;
		mFirstRequest = firstRequest;
	}
	
	
	public ClientIdentifier getClientIdentifier() {
		return mClientIdentifier;
	}
	
	
	public InputStream getRequestContent() {
		return mRequestContent;
	}
	
	public boolean isFirstRequest() {
		return mFirstRequest;
	}


	@Override
	public void dispatch(EventProcessor ep) {
		ep.processRequestChannelEvent(this);
	}
}
