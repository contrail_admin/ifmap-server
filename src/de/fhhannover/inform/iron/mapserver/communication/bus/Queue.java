/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus
 * File:    Queue.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Simple wrapper around an existing queue implementation.
 * I think, it is not really worth commenting this.
 * 
 * @author aw
 */
public class Queue<T> {
	
	private BlockingQueue<T> mQueue = new LinkedBlockingQueue<T>();
	
	
	public void put(T element) throws InterruptedException {
		NullCheck.check(element, "element is null");
		mQueue.put(element);
	}
	
	public T get() throws InterruptedException {
		return mQueue.take();
	}
	
	public boolean isEmpty() {
		return mQueue.isEmpty();
	}
}
