/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication.bus.messages
 * File:    Event.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication.bus.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;

/**
 * Simple interface for an event. The {@link EventProcessor} class needs to
 * implement the necessary processing methods for each {@link Event} implementation.
 * Using the {@link #dispatch(EventProcessor)} method, this allows for
 * a double dispatch mechanism, so we don't have to do all the crazy instanceof
 * comparisons.
 * 
 * @author aw
 */
public interface Event {
	
	/**
	 * Call appropriate method to process this {@link Event} implementation
	 * using the given {@link EventProcessor} object
	 * 
	 * @param ep
	 */
	public void dispatch(EventProcessor ep);
}
