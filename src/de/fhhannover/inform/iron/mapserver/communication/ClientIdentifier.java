/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication
 * File:    ClientIdentifier.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication;

import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Identifies a MAPC based on the credentials used during authentication
 * with the MAPS.
 * 
 * TODO: We could add information whether this Client uses basic authentication
 *       or certificate based authentication. Do we care?
 * 
 * @author aw
 *
 */
public class ClientIdentifier {
	
	
	/**
	 * Either the DN (distinguished name, separated by dots) or the username
	 * used during basic authentication should be stored here.
	 */
	private final String mValue;

	
	/**
	 * Cached hash code value
	 */
	private final int mHashCode;
	
	
	/**
	 * Constructs a {@link ClientIdentifier} object based on DN or username of a
	 * MAPC.
	 * 
	 * @param value
	 * @throws Exception
	 */
	public ClientIdentifier(String value) {
		
		NullCheck.check(value, "value is null");
		
		if (value.length() == 0) {
			throw new RuntimeException("length of ClientIdentifier value too short");
		}
		
		mValue = value;
		mHashCode = mValue.hashCode();
	}
	
	public String getStringValue() {
		return mValue;
	}

	@Override
	public int hashCode() {
		return mHashCode;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof ClientIdentifier) {
			return this.mValue.equals(((ClientIdentifier)o).getStringValue());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return mValue;
	}
}
