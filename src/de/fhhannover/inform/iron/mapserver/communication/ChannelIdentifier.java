/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.communication
 * File:    ChannelIdentifier.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.communication;

import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Identifies a channel based on ip:port and a counter value,
 * 
 * TODO: IPv4, IPv6 issue? In the end it doesn't matter, we use this class
 *       only to identify a channel. We don't care what kind of protocol is
 *       used.
 * 
 * TODO: Do we really need the counter? Or maybe we can only use the counter?
 * 
 * @author aw
 *
 */
public class ChannelIdentifier {

	/**
	 * Host IP address
	 */
	private final String mIpAddress;

	/**
	 * Represents the port used by the MAPC
	 */
	private final int mPort;
	
	/**
	 * Represents a simple counter value
	 */
	private final int mCounter;

	
	/**
	 * Cached hash code value
	 */
	private final int mHashCode;
	
	
	/**
	 * Construct a {@link ChannelIdentifier} object representing either a SSRC
	 * or ARC.
	 * 
	 * @param ip
	 * @param port
	 * @param counter
	 * @throws Exception
	 */
	public ChannelIdentifier(String ip, int port, int counter) {
		
		NullCheck.check(ip, "ip is null");
		
		if (ip.length() == 0) {
			throw new RuntimeException("length of ChannelIdentifier mIpAddress too short");
		}
		
		mIpAddress = ip;
		mPort = port;
		mCounter = counter;
		mHashCode = (mIpAddress + ":" + mPort + ":"+ mCounter).hashCode();
	}

	@Override
	public int hashCode() {
		return mHashCode;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		
		if (o instanceof ChannelIdentifier) {
			ChannelIdentifier ci = (ChannelIdentifier) o;
			return this.mIpAddress.equals(ci.mIpAddress)
				&& this.mPort == ci.mPort
				&& this.mCounter == ci.mCounter;
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return mIpAddress + ":" + mPort + ":" + mCounter;
	}
}
