/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.binding
 * File:    FilterFactory.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.binding;

import java.util.Map;

import de.fhhannover.inform.iron.mapserver.datamodel.search.Filter;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidFilterException;
import de.fhhannover.inform.iron.mapserver.utils.FilterValidator;

/**
 * Simple class to create a {@link Filter} object from the filter string
 * and namespace map.
 * 
 * @author aw
 * 
 * created 12.02.10
 * 
 * changes:
 * 19.02.10 aw - modified to be used with a map of namespace prefix to
 *               namespace uri. Deprecated transformFilter(String);
 *
 */
public class FilterFactory {
	
	private FilterFactory() { }
	
	/**
	 * Transformer method to create a filter object
	 * from a filterstring and a given namespace prefix
	 * namespace uri map.<br>
	 * 
	 * If fs is null a filterobject which matches everything
	 * is contstructed.<br>
	 * 
	 * If fs is empty, fs.length() == 0,  a filterobject which
	 * matches nothing is constructed.<br/>
	 * 
	 * Else fs is seen as a Filterstring specified in the IF-MAP
	 * specification.<br/>
	 * 
	 * If nm is null a default map with one entry<br>
	 * ["meta", "urn:trustedcomputinggroup.org:2010:IFMAP-METADATA:2" ]<br>
	 * is used. One will get a warning constructing such a filter.
	 * 
	 * @param fs filterstring
	 * @param nm namespace mapping
	 * @return
	 * @throws InvalidFilterException 
	 */
	public static Filter newFilter(String fs, Map<String, String> nm) throws InvalidFilterException {
		Filter f = new Filter(fs, nm);
		if (!FilterValidator.validateFilter(f))
			throw new InvalidFilterException("Bad Filter");
		return f;
	}
}
