/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.binding
 * File:    RequestUnmarshallerFactory.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.binding;

import de.fhhannover.inform.iron.mapserver.datamodel.meta.MetadataFactory;
import de.fhhannover.inform.iron.mapserver.provider.SchemaProvider;

/**
 * Simple abstract class which provides functionality to request a concrete
 * {@link RequestUnmarshaller} implementation.
 * 
 * @author aw
 */
public abstract class RequestUnmarshallerFactory {
	
	public static RequestUnmarshaller newRequestUnmarshaller(
			MetadataFactory metaFac, SchemaProvider schemaProvider) {
	
		// Ugly shortcut here...
		return new JaxbRequestUnmarshaller(metaFac, 
				SimpleValidationEventHandlerFactory.newInstance(),
				schemaProvider);
	}
}
