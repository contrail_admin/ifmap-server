/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.binding
 * File:    ResultMarshaller.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.binding;

import java.io.InputStream;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;
import de.fhhannover.inform.iron.mapserver.messages.Result;

/**
 * Interface to support marshalling of {@link Result} objects, independently
 * of the underlying marshalling method.
 * 
 * A {@link ResultMarshaller} implementation is used by the {@link EventProcessor}
 * to create {@link InputStream} objects from {@link Result} objects.
 * 
 * The {@link InputStream} objects content represents the appropriate representation
 * of the {@link Result} object in XML form. I.e. the {@link InputStream} objects
 * content can be used as the HTTP body in a response to a client.
 * 
 * @author aw
 */
public interface ResultMarshaller {
	
	/**
	 * Marshal a given {@link Result} object to the appropriate XML document
	 * returned as an {@link InputStream}.
	 * 
	 * Marshalling should be done <b>without</b> pretty print or any namespace
	 * optimization. The reason is that the {@link DataModelService} assumes
	 * identifiers to be send in the most compact form as possible and each
	 * {@link Metadata} object keeps it's local namespace declarations.
	 * 
	 * All possible {@link Result} implementations have to be supported. If
	 * a {@link ResultMarshaller} implementation does not recognize a given
	 * implementation it should throw a {@link RuntimeException}. The
	 * {@link ResultMarshaller} has to be fixed in this case.
	 * 
	 * 
	 * @param result any possible {@link Result} implementation.
	 * @return an {@link InputStream} containing the resulting XML document
	 */
	InputStream marshal(Result result);
}
