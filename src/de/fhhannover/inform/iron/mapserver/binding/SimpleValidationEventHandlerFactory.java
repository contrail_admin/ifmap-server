/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.binding
 * File:    SimpleValidationEventHandlerFactory.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.binding;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;

/**
 * Provides {@link ValidationEventHandler} handler.
 * 
 * Currently using a inner class to do so...
 * 
 * @since 0.3.0
 * @author aw
 *
 */
class SimpleValidationEventHandlerFactory implements
		ValidationEventHandlerFactory {
	
	private SimpleValidationEventHandlerFactory() { }
	
	public static SimpleValidationEventHandlerFactory newInstance() {
		return new SimpleValidationEventHandlerFactory();
	}
	
	@Override
	public SimpleValidationEventHandler newValidationEventHandler() {
		return new SimpleValidationEventHandlerImpl();
	}
	
	private class SimpleValidationEventHandlerImpl implements SimpleValidationEventHandler {
		
		
		private boolean mErrorOccurred = false;
		private String mErrorMessage;

		@Override
		public boolean handleEvent(ValidationEvent event) {
			int eventSeverity = event.getSeverity();
			
			// We always bail out on fatal errors...
			if (eventSeverity == ValidationEvent.FATAL_ERROR ||
					eventSeverity == ValidationEvent.ERROR) {
				mErrorOccurred = true;
				mErrorMessage = event.getMessage();
			}
			
			return !mErrorOccurred;
		}

		@Override
		public boolean hasErrorOccured() {
			return mErrorOccurred;
		}
	
		@Override
		public String getErrorMessage() {
			return mErrorMessage;
		}
	}
}
