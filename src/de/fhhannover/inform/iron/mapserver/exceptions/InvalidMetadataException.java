/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.exceptions
 * File:    InvalidMetadataException.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.exceptions;

/**
 * Exception which is thrown if a MAPC tries to publish singleValue metadata,
 * but the same identifier already has multiValue metadata attached to it
 * (or the other way round).
 * 
 * @version 0.1
 * 
 * @author aw
 * 
 * created: 30.09.10
 * changes:
 *  30.09.10 aw - Created first version of this class
 *
 */
public class InvalidMetadataException extends Exception {

	/**
	 * auto-generated
	 */
	private static final long serialVersionUID = -794475694229081749L;
	
	public InvalidMetadataException(String message) {
		super(message);
	}

}
