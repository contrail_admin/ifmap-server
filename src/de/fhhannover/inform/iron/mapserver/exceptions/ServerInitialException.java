/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.exceptions
 * File:    ServerInitialException.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.exceptions;

/**
 * This exception class is used during the initialization of the map server. 
 * 
 * @author Waldemar Bender
 * @author Dennis Ludewig
 *
 */
public class ServerInitialException extends Exception {

	private static final long serialVersionUID = 3623464090698140992L;

	public ServerInitialException(String msg){
		super(msg);
	}
}
