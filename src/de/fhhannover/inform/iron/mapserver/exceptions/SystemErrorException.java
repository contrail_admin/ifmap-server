/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.exceptions
 * File:    SystemErrorException.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.exceptions;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.messages.ErrorCode;
import de.fhhannover.inform.iron.mapserver.messages.ErrorResult;

/**
 * To be used instead if something goes terrible wrong.
 * 
 * If the {@link EventProcessor} sees this one, we should send out an
 * {@link ErrorCode#SystemError} to the MAPC {@link ErrorResult} and
 * shut the f*** down..
 * 
 * @since 0.3.0
 * @author aw
 */
public class SystemErrorException extends RuntimeException {
	/**
	 * auto-generated
	 */
	private static final long serialVersionUID = 5458391006303890599L;

	public SystemErrorException(String string) {
		super(string);
	}
}
