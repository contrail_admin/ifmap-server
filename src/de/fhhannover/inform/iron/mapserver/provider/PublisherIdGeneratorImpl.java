/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    PublisherIdGeneratorImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import java.util.Set;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.utils.CollectionHelper;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Simple implementation of the {@link PublisherIdGenerator} interface.
 * A little bit ugly :-(
 * We take the string value from the {@link ClientIdentifier}, add
 * some numbers which we got from currentTimeMillis and an additional
 * counter.
 * 
 * @author aw
 *
 */
public class PublisherIdGeneratorImpl implements PublisherIdGenerator {

	@Override
	public String generatePublisherIdFor(ClientIdentifier clientId,
			PublisherIdProvider pubIdProv) {
		NullCheck.check(clientId, "clientId is null");
		NullCheck.check(pubIdProv, "pubIdProv is null");
	
		int counter = 0;
		int time = (int) System.currentTimeMillis();
		String genPubId = null;
		
		// allow for faster lookups
		Set<String> set = CollectionHelper.provideSetFor(String.class);
		set.addAll(pubIdProv.getAllPublisherIds());
		
		do {
			counter++;
			genPubId = clientId.getStringValue() + "-" + time + "-" + counter;
		} while (set.contains(genPubId));
		
		return genPubId;
	}
}
