/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    IncremetalSessionIdProviderImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

/**
 * TODO: The session-id is simply incremented on each call.
 * No, this is not conform with the specification!
 * 
 * @author aw
 *
 */
public class IncremetalSessionIdProviderImpl implements SessionIdProvider {
	
	private static int sCurrentValue = -1;

	@Override
	public String getSessionId() {
		
		// handle overflow...
		if (sCurrentValue < 0) {
			sCurrentValue = -1;
		}
		return "" + ++sCurrentValue;
	}

}
