/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    ServerConfigurationProvider.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import de.fhhannover.inform.iron.mapserver.communication.http.ActionProcessor;
import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;


/**
 * Interface to get configuration information about the server.
 *
 * TODO: Extend this interface with whatever you need.
 * 
 * @author aw
 * 
 */
public interface ServerConfigurationProvider extends DataModelServerConfigurationProvider {
	
	/**
	 * @return the port the server should listen on for basic authentication.
	 */
	public int getBasicAuthPort();

	/**
	 * @return the port the server should listen on for certificate authentication.
	 */
	public int getCertAuthPort();
	
	/**
	 * @return path to the truststore
	 */
	public String getTrustStoreFileName();
	
	/**
	 * @return password of the truststore
	 */
	public String getTrustStorePasswort();

	
	/**
	 * @return path to the keystore
	 */
	public String getKeyStoreFileName();
	
	/**
	 * @return passwort of the keystore
	 */
	public String getKeyStorePasswort();
	
	
	/**
	 * @return information to initialize a {@link PublisherIdProvider}
	 */
	public String getPublisherIdMapFileName();
	
	/**
	 * @return information to initialize a {@link BasicAuthProvider}
	 */
	public String getBasicAuthenticationPropFileName();

	/**
	 * @return the number of forwarder threads to be used in the {@link EventProcessor}
	 */
	public int getEventProcessorForwardersCount();

	/**
	 * @return the number of worker threads to be used in the {@link EventProcessor}
	 */
	public int getEventProcessorWorkersCount();

	/**
	 * @return the number of forwarder threads to be used in the {@link ActionProcessor};
	 */
	public int getActionProcessorForwardersCount();

	/**
	 * @return the number of worker threads to be used in the {@link ActionProcessor};
	 */
	public int getActionProcessorWorkersCount();

	/**
	 * @return the timeout for a session if it has no active SSRC in mili seconds.
	 */
	public long getSessionTimeOutMilliSeconds();
	
	/**
	 * @return to initialize a {@link AuthorizationProvider}
	 */
	public String getAuthorizationPropFileName();

	/**
	 * @return true if raw requests should be logged
	 */
	public boolean isLogRaw();

	/**
	 * @return which error should be used to abort parsing.
	 */
	public boolean getXmlValidation();

	/**
	 * @return all schema files to be loaded to do XML validation
	 */
	public String[] getSchemaFileNames();
}

