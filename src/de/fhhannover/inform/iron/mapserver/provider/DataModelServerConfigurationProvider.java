/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    DataModelServerConfigurationProvider.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;

/**
 * Configuration options specific for the {@link DataModelService}
 * 
 * @author aw
 *
 */
public interface DataModelServerConfigurationProvider {
	
	/**
	 * @returns whether this identity type is to be treated case sensitive
	 */
	public boolean getIdentityTypeIsCaseSensitive(String identityType);
	
	/**
	 * @return whether the administrative domain is to be treated case sensitive
	 */
	public boolean getAdministrativeDomainIsCaseSensitive();
	
	/**
	 * @return whether the purge publisher operation is restricted
	 */
	public boolean getPurgePublisherIsRestricted();
	
	/**
	 * @return default max poll result size to be used
	 */
	public int getDefaultMaxPollResultSize();
	

	/**
	 * @return the number of bytes used as default max search result size
	 */
	public int getDefaultMaxSearchResultSize();
	
	/**
	 * @return true if certain validations and/or null checks should be enabled. 
	 */
	public boolean isSanityChecksEnabled();
}
