/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    BasicAuthProviderPropImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import java.io.IOException;

import de.fhhannover.inform.iron.mapserver.exceptions.ProviderInitializationException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Implementation of the BasicAuthProvider.
 *	
 *
 * @author tr
 * 
 * aw: Did some restructuring... Properties file gets read only once now.
 *     Had some bug with multiple authenticate() calls. Further, changed the
 *     interface to what I had in mind, moving the Base64 stuff upwards.
 *     So we are not dependent on apache stuff down here. Throw a
 *     {@link ProviderInitializationException} if something goes wrong.
 */
public class BasicAuthProviderPropImpl implements BasicAuthProvider {

	private String mPropertiesFileName;

	
	public BasicAuthProviderPropImpl(ServerConfigurationProvider serverConf)
										throws ProviderInitializationException{
		loadPropertiesFile(serverConf);
	}
	
	@Override
	public boolean verify(String username, String password) {
		try {
			PropertiesReaderWriter props = new PropertiesReaderWriter(mPropertiesFileName, true);
			String passFile = props.getProperty(username);
			return password.equals(passFile);
		} catch (IOException e) {
			return false;
		}

	}

	private void loadPropertiesFile(ServerConfigurationProvider serverConf)
										throws ProviderInitializationException {
		NullCheck.check(serverConf, "serverConf is null");
		try {
			String fileName = serverConf.getBasicAuthenticationPropFileName();
			if (fileName == null) {
				throw new ProviderInitializationException(
						"basic auth user/pass file not given");
			}
			PropertiesReaderWriter testProps = new PropertiesReaderWriter(fileName, true);
                        mPropertiesFileName = fileName;
		} catch (IOException e) {
			throw new ProviderInitializationException(e.getMessage());
		}
	}
}
