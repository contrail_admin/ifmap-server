/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    PublisherIdProviderPropImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.exceptions.ProviderInitializationException;
import de.fhhannover.inform.iron.mapserver.exceptions.StorePublisherIdException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * TODO: For the moment this simply returns the clientIdentifier's string value,
 * but we have to work with a {@link Properties} file to find the mapping.
 * If there is no mapping available, create a new one.
 * 
 * @author aw
 *
 */
public class PublisherIdProviderPropImpl implements PublisherIdProvider {
	
	private PropertiesReaderWriter mProperties;

	public PublisherIdProviderPropImpl(ServerConfigurationProvider serverConfig) 
		throws ProviderInitializationException {
		NullCheck.check(serverConfig, "serverConfig is null");
		String fileName = serverConfig.getPublisherIdMapFileName();
		if (fileName == null) {
			throw new ProviderInitializationException("publisher-id mapping file null");
		}
		
		try {
			mProperties = new PropertiesReaderWriter(fileName, true);
		} catch (IOException e) {
			throw new ProviderInitializationException(e.getMessage());
		}
	}
	
	@Override
	public String getPublisherIdFor(ClientIdentifier clientIdentifier) {
		NullCheck.check(clientIdentifier, "clientIdentifier is null");
		return mProperties.getProperty(clientIdentifier.getStringValue());
	}
	
	@Override
	public void storePublisherIdFor(ClientIdentifier clientId,
			String publisherId) throws StorePublisherIdException {
		NullCheck.check(clientId, "clientId is null");
		NullCheck.check(publisherId, "publisherId is null");
		try {
			mProperties.storeProperty(clientId.getStringValue(), publisherId);
		} catch (IOException e) {
			throw new StorePublisherIdException(e.getMessage());
		}
	}

	@Override
	public List<String> getAllPublisherIds() {
		return mProperties.getAllValues();
	}
}
