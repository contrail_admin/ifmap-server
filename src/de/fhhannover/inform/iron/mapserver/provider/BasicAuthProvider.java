/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    BasicAuthProvider.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import java.util.Properties;

/**
 * Provides basic authentication functionality.
 * 
 * A MAPC can authenticate using certificates or basic authentication, this
 * interface allows for verifying if a client used basic authentication.
 * 
 * 
 * @author aw
 *
 */
public interface BasicAuthProvider {
	
	/**
	 * The Authorization HTTP header fields value is base64(username":"password).
	 * 
	 * A implementation of this method has to check whether this username
	 * password combination is valid. For example, username and password
	 * combinations could be stored in a {@link Properties} file.
	 * 
	 * @param username the username
	 * @param password the used password
	 * @return true if the user could be authenticated, else false
	 */
	public boolean verify(String username, String password);

}
