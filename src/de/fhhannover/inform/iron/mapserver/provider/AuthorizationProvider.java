/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    AuthorizationProvider.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;

/**
 * Provides access to authorization information for MAPC.
 * 
 * This is a preliminary version as it only allows the differentiation of
 * MAPC into read-only and read-write clients. If a MAPC is not read-write,
 * then it is read-only.<br/><br/>
 * 
 * The specification provides an example of a more fine grained authorization
 * model (allowing a DHCP MAPC only to publish <ip-mac> metadata on ip-mac links.
 * <br/>
 * Such a model might be considered in the future.
 * <br/><br/>
 * 
 * 
 * <b>Note: publish notify is considered a write operation.</b>
 * 
 * @author aw
 *
 */
public interface AuthorizationProvider {
	
	
	/**
	 * Check whether a MAPC is allowed change the content of the graph, I.e.
	 * publish or delete metadata, make notifications, do a purgePublisher.
	 * 
	 * @param clientId
	 * @return true if the MAPC is allowed to change the graph content.
	 */
	public boolean isWriteAllowed(ClientIdentifier clientId);

}
