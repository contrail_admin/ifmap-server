/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    PublisherIdGenerator.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;

/**
 * Interface to abstract from the generation of publisher-id's.
 * 
 * Section 3.3.1
 * 
 * @author aw
 *
 */
public interface PublisherIdGenerator {
	
	/**
	 * Generate a new publisher-id for the given {@link ClientIdentifier}.
	 * 
	 * The {@link PublisherIdProvider} parameter is provided in order to be able
	 * to check, whether the generated publisher-id is already used.
	 * 
	 * @param clientId identifies the MAPC
	 * @param pubIdProv can be used to check whether this 
	 * @return a publisher-id which isn't stored in the PublisherIdProvider instance
	 *         already
	 */
	public String generatePublisherIdFor(ClientIdentifier clientId, PublisherIdProvider pubIdProv);
}
