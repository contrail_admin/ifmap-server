/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.provider
 * File:    PublisherIdProvider.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.provider;

import java.util.List;
import java.util.Properties;

import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.exceptions.StorePublisherIdException;

/**
 * Provides functionality to map an identified MAPC to a PublisherId.
 * 
 * A MAPC can be identified either by it's username used for basic authentication
 * or by it's distinguished name used in the certificate.
 * This interface provides a mapping between these identifiers and the
 * publisher-id.
 * 
 * 
 * @author aw
 *
 */
public interface PublisherIdProvider {
	
	/**
	 * Returns a PublisherId for the MAPC identified by the
	 * {@link ClientIdentifier}.
	 * 
	 * The easiest implementation should be a {@link Properties} file mapping
	 * username/distinguished name to a PublisherId.
	 * 
	 * @see ClientIdentifier
	 * @param clientId identifies the MAPC.
	 * @return a String representing the publisher-id or null if not found.
	 */
	public String getPublisherIdFor(ClientIdentifier clientId);
	
	/**
	 * Store a mapping of a {@link ClientIdentifier} to a publisher-id.
	 * 
	 * @param clientId
	 * @param publisherId
	 * @throws StorePublisherIdException in case it could not be stored.
	 */
	public void storePublisherIdFor(ClientIdentifier clientId, String publisherId)
		throws StorePublisherIdException;
	
	/**
	 * @return a {@link List} containing all publisher-id's known to
	 * this instance.
	 */
	public List<String> getAllPublisherIds();
	
}
