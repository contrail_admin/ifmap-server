/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    PurgePublisherRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.exceptions.AccessDeniedException;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * Class which represents the PurgePublishRequest which is a top level
 * request.
 * 
 * @author aw
 * @version 0.1
 */ 

/*
 * created: 17.12.09
 * changes:
 *  17.12.09 aw - created first version of this class
 *  05.02.10 aw - throw RequestCreationException, use Request super class
 *  02.12.10 aw - Use RequestWithSessionId as superclass, make publisherId final
 */
public class PurgePublisherRequest extends RequestWithSessionId {
	
	/**
	 * Indicates which publisher to flush
	 */
	private final String publisherId;
	
	PurgePublisherRequest(String sessionid, String publisherid) throws RequestCreationException {
		super(sessionid);
		
		if (publisherid == null || publisherid.length() == 0) {
			throw new RequestCreationException("publisherId is not set");
			
		}
		publisherId = publisherid;
	}
	
	public String getPublisherId() {
		return publisherId;
	}

	@Override
	public void dispatch(EventProcessor ep) throws AccessDeniedException {
		ep.processPurgePublisherRequest(this);
		
	}
}
