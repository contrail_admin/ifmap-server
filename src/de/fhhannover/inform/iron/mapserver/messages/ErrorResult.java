/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    ErrorResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.messages.ErrorCode;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Represents a errorResult.
 * 
 * We allow a null errorString, which means no errorString is to be set.
 * 
 * 
 * @author aw
 *
 */
public class ErrorResult extends Result {
	
	private final ErrorCode mErrorCode;
	private final String mErrorString;

	public ErrorResult(ChannelIdentifier channelId, ClientIdentifier clientId,
			ErrorCode errCode, String errorString) {
		super(channelId, clientId);
		NullCheck.check(errCode, "error code is null");
		mErrorCode = errCode;
		mErrorString = errorString;
	}
	
	public ErrorResult(ChannelIdentifier channelId, ClientIdentifier clientId,
			ErrorCode errCode) {
		this(channelId, clientId, errCode, null);
	}

	public ErrorCode getErrorCode() {
		return mErrorCode;
	}

	public String getErrorString() {
		return mErrorString;
	}

}
