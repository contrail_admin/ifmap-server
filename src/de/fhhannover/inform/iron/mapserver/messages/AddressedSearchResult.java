/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    AddressedSearchResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;
import de.fhhannover.inform.iron.mapserver.datamodel.search.SearchResult;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Class encapsulating a {@link SearchResult} to include a {@link ChannelIdentifier}
 * to specify where the {@link SearchResult} has to be sent to.
 * 
 * We do it this way round so the {@link DataModelService} needs not to know
 * about {@link ChannelIdentifier} and so on and we can still reuse the
 * the implementation.
 * 
 * @author aw
 *
 */
public class AddressedSearchResult extends Result {
	
	private final SearchResult mSearchResult;

	public AddressedSearchResult(ChannelIdentifier channelId,
			ClientIdentifier clientId, SearchResult sr) {
		super(channelId, clientId);
		NullCheck.check(sr, "pr is null");
		mSearchResult = sr;
	}
	
	public SearchResult getSearchResult() {
		return mSearchResult;
	}
}
