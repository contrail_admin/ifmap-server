/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    AddressedPollResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;
import de.fhhannover.inform.iron.mapserver.datamodel.search.PollResult;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Class encapsulating a {@link PollResult} to include a {@link ChannelIdentifier}
 * to specify where the {@link PollResult} has to be sent to.
 * 
 * We do it this way round so the {@link DataModelService} needs not to know
 * about {@link ChannelIdentifier} and so on and we can still reuse the
 * the implementation.
 * 
 * @author aw
 *
 */
public class AddressedPollResult extends Result {
	
	private final PollResult mPollResult;

	public AddressedPollResult(ChannelIdentifier channelId,
			ClientIdentifier clientId, PollResult pr) {
		super(channelId, clientId);
		NullCheck.check(pr, "pr is null");
		mPollResult = pr;
	}
	
	public PollResult getPollResult() {
		return mPollResult;
	}
}
