/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    Result.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;


import de.fhhannover.inform.iron.mapserver.communication.ChannelIdentifier;
import de.fhhannover.inform.iron.mapserver.communication.ClientIdentifier;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * Abstract class which contains a {@link ChannelIdentifier}. where the
 * corresponding response has to be sent to.
 * 
 * @author aw
 *
 */
public abstract class Result {
	
	/**
	 * Represents the "address" where the result has to be send.
	 */
	private final ChannelIdentifier mChannelIdentifier;
	private final ClientIdentifier mClientIdentifier;

	public Result(ChannelIdentifier channelId, ClientIdentifier clientId) {
		NullCheck.check(channelId, "Channel Identifier not given");
		NullCheck.check(clientId, "Client Identifier not given");
		mChannelIdentifier = channelId;
		mClientIdentifier = clientId;
	}
	
	
	public ChannelIdentifier getChannelIdentifier() {
		return mChannelIdentifier;
	}


	public ClientIdentifier getClientIdentifier() {
		return mClientIdentifier;
	}
}
