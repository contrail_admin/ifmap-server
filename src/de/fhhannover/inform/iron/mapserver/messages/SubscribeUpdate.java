/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    SubscribeUpdate.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.datamodel.search.Subscription;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * Simple Request to update a {@link Subscription}.
 * This needs a {@link SearchRequest} as parameter. A subscription is described
 * by a {@link SearchRequest}.
 * Checks about the validity of a {@link SearchRequest} is done before through
 * construction of such a request.
 * 
 * @version 0.1
 * @author aw
 */

/*
 * created: 30.04.10
 * 
 * changes:
 * 
 *  30.04.10 aw - ...
 *  08.06.10 aw - constructor package visibility,
 *  			  changed n to name and sr to searchrequest
 * 
 * 
 */
public class SubscribeUpdate  extends SubSubscribeRequest {
	
	private SearchRequest searchRequest;

	SubscribeUpdate(String name, SearchRequest searchrequest)
												throws RequestCreationException {
		super(name);
		
		if (searchrequest == null)
			throw new RequestCreationException("Search Request is null");
		
		searchRequest = searchrequest;
	}
	
	public SearchRequest getSearchRequest() {
		return searchRequest;
	}
}
