/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    EndSessionRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * This request is processed if a MAPC terminates a connection
 * or if it times out.
 * 
 * @author aw
 * @version 0.1
 */

/* 
 * created: 05.12.09
 * changes:
 * 	05.12.09 aw - First version of this class.
 *  05.02.10 aw - Added RequestCreationException, use Request superclass
 *  02.12.10 aw - Use RequestWithSessionId as superclass
 *  
 *
 */
public class EndSessionRequest extends RequestWithSessionId {
 
	EndSessionRequest(String sid) throws RequestCreationException {
		super(sid);
	}

	@Override
	public void dispatch(EventProcessor ep) {
		ep.processEndSessionRequest(this);
	}
}
 
