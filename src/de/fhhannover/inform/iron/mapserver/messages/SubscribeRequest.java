/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    SubscribeRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import java.util.List;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.exceptions.AccessDeniedException;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * A Subscription Request can contain a number of
 * {@link SubscriptionDelete} and {@link SubscriptionUpdate} elements
 * which are stored in a list of {@link SubSubscriptionRequest}s
 * 
 * @version 0.1
 * @author aw
 */

/*
 * created: 30.04.10
 * 
 * changes:
 *  30.04.10 aw - ...
 *  02.12.10 aw - Use RequestWithSessionId as superclass
 */
public class SubscribeRequest extends RequestWithSessionId {
	
	private List<SubSubscribeRequest> ssrList;

	SubscribeRequest(String sessionId, List<SubSubscribeRequest> ssrlist)
												throws RequestCreationException {
		super(sessionId);
		
		if (ssrlist == null || ssrlist.size() == 0)
			throw new RequestCreationException("No SubSubscriptionRequests given");
		
		ssrList = ssrlist;
	}
	
	public List<SubSubscribeRequest> getSubSubscribeRequests() {
		return ssrList;
	}

	@Override
	public void dispatch(EventProcessor ep) throws AccessDeniedException {
		ep.processSubscribeRequest(this);
		
	}
}
