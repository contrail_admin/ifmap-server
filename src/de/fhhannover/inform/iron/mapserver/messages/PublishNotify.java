/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    PublishNotify.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import java.util.List;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * PublishNotify is send in order to send Notifies. It's exactly like
 * a PublishUpdate
 * 
 * @version 0.1
 * @author aw
 */

 /* 
 * created: 30.04.10
 * changes:
 *  30.04.10 aw - implement some stuff
 * 	
 */
public class PublishNotify extends PublishUpdate {
	
	/**
	 * Construct a PublishNotify exactly how {@link PublishUpdate} is
	 * constructed.
	 * 
	 * @param i1 Identifier 1
	 * @param i2 Identifier 2
	 * @param ml List of metadata
	 * @param timeStamp timeStamp of this request
	 * @throws RequestCreationException
	 */
	public PublishNotify(Identifier i1, Identifier i2, List<Metadata> ml)
			throws RequestCreationException {
		super(i1, i2, ml);
	}
	public PublishNotify(Identifier i1, List<Metadata> ml)
			throws RequestCreationException {
		this(i1, null, ml);
	}
}
 
