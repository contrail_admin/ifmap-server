/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    NewSessionRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;
import de.fhhannover.inform.iron.mapserver.exceptions.AccessDeniedException;

/**
 * A request that needs to be given to the {@link DataModelService} if a
 * MAPC creates a new connection.
 * 
 * @author aw
 * @version 0.1
 */

/* 
 * created: 05.12.09
 * changes:
 * 	05.12.09 aw - Created class, well it was created before by wb, but he
 * 		didn't comment that.
 * 
 *	17.12.09 aw - Constructors will throw exceptions.
 *  05.02.10 aw - Changed exception to RequestCreationException,
 *                use Request superclass
 *  02.12.10 aw - remove everything except the maxPollResultSize
 *
 */
public class NewSessionRequest extends Request {
 
	private final Integer maxPollResultSize;
	
	NewSessionRequest(Integer maxpollsize) {
		maxPollResultSize = maxpollsize;
	}
	
	public Integer getMaxPollResultSize() {
		return maxPollResultSize;
	}

	@Override
	public void dispatch(EventProcessor ep) throws AccessDeniedException {
		ep.processNewSessionRequest(this);
	}
}
 
