/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    DumpResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import java.util.Collection;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;

/**
 * Contains all linked {@link Identifier}
 *
 *
 * @author tr
 *
 */
public class DumpResult {
	
	private Collection<Identifier> mIdentifiers;
	private String mFilter;
	private long mLastUpdateTime;
	
	public DumpResult() { }
	
	public long getLastUpdateTime() {
		return mLastUpdateTime;
	}

	public void setLastUpdateTime(long lastUpdateTime) {
		mLastUpdateTime = lastUpdateTime;
	}

	public String getFilter(){
		return mFilter;
	}
	
	public void setFilter(String filter) {
		mFilter = filter;
	}

	public Collection<Identifier> getIdentifier() {
		return mIdentifiers;
	}

	public void setIdentifier(Collection<Identifier> identifiers) {
		mIdentifiers = identifiers;
	}
}
