/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.messages
 * File:    DumpRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.messages;

import de.fhhannover.inform.iron.mapserver.communication.ifmap.EventProcessor;
import de.fhhannover.inform.iron.mapserver.exceptions.RequestCreationException;

/**
 * A {@link Request} implementation representing the <i>special</i> dump
 * operation.
 * 
 * @author aw
 */
public class DumpRequest extends RequestWithSessionId {
	
	private final String dumpIdentifier;

	DumpRequest(String sid, String identifier) throws RequestCreationException {
		super(sid);
		if (identifier == null) {			
			dumpIdentifier = "*";
		}
		else {
			dumpIdentifier = identifier;
		}
	}
	
	/**
	 * @return not an IF-MAP identifier, I think?
	 */
	public String getIdentifier() {
		return dumpIdentifier;
	}

	@Override
	public void dispatch(EventProcessor ep) {
		ep.processDumpRequest(this);
	}

}
