/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel
 * File:    SubscriptionNotifier.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel;

import de.fhhannover.inform.iron.mapserver.datamodel.search.PollResult;
import de.fhhannover.inform.iron.mapserver.exceptions.AlreadyObservedException;
import de.fhhannover.inform.iron.mapserver.exceptions.NoPollResultAvailableException;
import de.fhhannover.inform.iron.mapserver.exceptions.PollResultsTooBigException;

/**
 * Interface for the {@link SubscriptionNotifier} and {@link SubscriptionObserver}
 * mechanism. This interface is implemented by the {@link DataModelService}
 * 
 * @version 0.1
 * @author aw
 *
 */
public interface SubscriptionNotifier {
	
	/**
	 * Register a {@link SubscriptionObserver} with this {@link SubscriptionNotifier}.
	 * Only one observer is allowed. If this method is called twice an exception
	 * is thrown. A {@link SubscriptionObserver} will be notified whenever a new
	 * @link PollResult} gets available for a given session. See the documentation
	 * of {@link SubscriptionObserver}.
	 * 
	 * @param subObs
	 * @throws AlreadyObservedException
	 */
	public void registerSubscriptionObserver(SubscriptionObserver subObs)
		throws AlreadyObservedException;

	
	
	/**
	 * This method returns a {@link PollResult} for the given session to the
	 * caller. If no {@link PollResult} is available a {@link NoPollResultAvailableException}
	 * is thrown. If the {@link PollResult} exceeded the given max-poll-result-size
	 * an {@link PollResultsTooBigException} is thrown.See the documentation of
	 * {@link SubscriptionObserver} when a call to this method makes sense.
	 * 
	 * @param sessionId
	 * @return
	 * @throws NoPollResultAvailableException
	 * @throws PollResultsTooBigException 
	 */
	public PollResult getPollResultFor(String sessionId)
		throws NoPollResultAvailableException, PollResultsTooBigException;

}
