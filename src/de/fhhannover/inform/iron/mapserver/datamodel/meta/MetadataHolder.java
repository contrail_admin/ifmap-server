/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataHolder.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;

import de.fhhannover.inform.iron.mapserver.datamodel.Publisher;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;

/**
 * A {@link MetadataHolder} encapsulates a real {@link Metadata} object and
 * offers access to the {@link Publisher} instance, {@link GraphElement}
 * instance, time-stamp and further administrative attributes.
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface MetadataHolder {
	
	/**
	 * @return the {@link Metadata} instance that is encapsulated by this
	 * 			{@link MetadataHolder} instance.
	 */
	public Metadata getMetadata();
	
	/**
	 * @return the {@link GraphElement} instance this {@link MetadataHolder}
	 * 			instance is attached to.
	 */
	public GraphElement getGraphElement();
	
	/**
	 * @return the {@link Publisher} instance indicating who published this
	 * 			{@link MetadataHolder} instance.
	 */
	public Publisher getPublisher();
	
	/**
	 * @return the state this {@link MetadataHolder} instance is in.
	 */
	public MetadataState getState();

	/**
	 * Set the sate of this {@link MetadataHolder} instance.
	 * @param state
	 */
	public void setState(MetadataState state);
	
	public boolean isNotify();
	
	public boolean isNew();
	
	public boolean isDeleted();
	
	public boolean isUnchanged();
	
	/**
	 * @return the lifetime of the attached {@link Metadata} object.
	 */
	public MetadataLifeTime getLifetime();

}
