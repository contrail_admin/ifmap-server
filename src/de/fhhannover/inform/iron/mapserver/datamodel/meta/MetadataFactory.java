/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataFactory.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.fhhannover.inform.iron.mapserver.exceptions.InvalidMetadataException;

/**
 * Create a {@link Metadata} objects based on a {@link Document} instances.
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface MetadataFactory {
	
	/**
	 * Return a {@link Metadata} instance with the contents given by the
	 * {@link Element} instance.
	 * 
	 * Looks at the name of the root element and the namespace and gets
	 * an corresponding {@link MetadataType} instance.
	 * 
	 * @param root
	 * @return
	 */
	Metadata newMetadata(Element root) throws InvalidMetadataException;

}
