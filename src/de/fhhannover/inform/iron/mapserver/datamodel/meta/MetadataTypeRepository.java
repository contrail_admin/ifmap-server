/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataTypeRepository.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;

import de.fhhannover.inform.iron.mapserver.exceptions.InvalidMetadataException;

/**
 * Stores {@link MetadataType} instances for given namespace / name value
 * pairs of {@link Metadata}.
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface MetadataTypeRepository {

	/**
	 * Query the repository for a name:ns mapping and return the
	 * {@link MetadataType} instance if lifetime is the same as
	 * already stored. Otherwise, create a new entry.
	 * 
	 * @param ns
	 * @param name
	 * @param card
	 * @return
	 * @throws InvalidMetadataException if the lifetime is different from the
	 * 			already stored entry.
	 */
	public MetadataType getTypeFor(String ns, String name, MetaCardinalityType card)
			throws InvalidMetadataException;
	
	/**
	 * Returns true if such a mapping exists.
	 * 
	 * @param name
	 * @param ns
	 * @param card
	 * @return
	 */
	public boolean contains(String ns, String name, MetaCardinalityType card);
	
	/**
	 * Clears all entries of the repository.
	 */
	public void clear();

}
