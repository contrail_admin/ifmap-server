/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataFactoryImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;


import org.w3c.dom.Element;

import de.fhhannover.inform.iron.mapserver.exceptions.InvalidMetadataException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

public class MetadataFactoryImpl implements MetadataFactory {

	private static final String CARDINALITYSTRING = "ifmap-cardinality";
	
	private final MetadataTypeRepository mTypeRep;
	
	private MetadataFactoryImpl(MetadataTypeRepository typeRep) {
		NullCheck.check(typeRep, "typeRep is null");
		mTypeRep = typeRep;
	}
	
	public static MetadataFactory newInstance(MetadataTypeRepository typeRep) {
		return new MetadataFactoryImpl(typeRep);
	}

	@Override
	public Metadata newMetadata(Element root) throws InvalidMetadataException {

		String name = null, ns = null;
		String cardString = null;
		MetaCardinalityType card = null;
		
		if (root == null)
			throw new InvalidMetadataException("no root element?");
	
		// Get the local name, if this returns null, we get the name
		name = root.getLocalName();
		name = (name == null) ? root.getNodeName() : name;
		
		ns = (root.getNamespaceURI() == null) ? "" : root.getNamespaceURI();
		
		if (name == null || name.length() == 0)
			throw new InvalidMetadataException("Bad metadata root elment?");

		cardString = root.getAttribute(CARDINALITYSTRING);
		
		if (cardString == null || cardString.length() == 0)
			throw new InvalidMetadataException("No ifmap-cardinality given?");

		try {
			card = MetaCardinalityType.valueOf(cardString);
		} catch (IllegalArgumentException e) {
			throw new InvalidMetadataException("Bad ifmap-cardinality ("
					+ cardString + ")");
		}
		
		return new W3cXmlMetadata(root, mTypeRep.getTypeFor(ns, name, card));
	}
}
