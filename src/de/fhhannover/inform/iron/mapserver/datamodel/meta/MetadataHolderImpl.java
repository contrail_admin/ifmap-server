/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataHolderImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;

import de.fhhannover.inform.iron.mapserver.datamodel.Publisher;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * 
 * @since 0.3.0
 * @author aw
 *
 */
class MetadataHolderImpl implements MetadataHolder {
	
	private final Metadata mMetadata;
	private final MetadataLifeTime mLifeTime;
	private final Publisher mPublisher;
	private final GraphElement mGraphElement;
	
	private MetadataState mState;
	
	MetadataHolderImpl(Metadata m, MetadataLifeTime lt, GraphElement ge, Publisher p) {
		NullCheck.check(m, "metadata is null");
		NullCheck.check(lt, "lifetime is null");
		NullCheck.check(ge, "graphElement is null");
		NullCheck.check(p, "publisher is null");
		
		mMetadata = m;
		mLifeTime = lt;
		mGraphElement = ge;
		mPublisher = p;
	}

	@Override
	public Metadata getMetadata() {
		return mMetadata;
	}

	@Override
	public GraphElement getGraphElement() {
		return mGraphElement;
	}

	@Override
	public Publisher getPublisher() {
		return mPublisher;
	}

	@Override
	public MetadataState getState() {
		return mState;
	}

	@Override
	public void setState(MetadataState state) {
		mState = state;
	}

	@Override
	public boolean isNotify() {
		return mState == MetadataState.NOTIFY;
	}

	@Override
	public boolean isNew() {
		return mState == MetadataState.NEW;
	}

	@Override
	public boolean isDeleted() {
		return mState == MetadataState.DELETED;
	}

	@Override
	public boolean isUnchanged() {
		return mState == MetadataState.UNCHANGED;
	}

	@Override
	public MetadataLifeTime getLifetime() {
		return mLifeTime;
	}
}
