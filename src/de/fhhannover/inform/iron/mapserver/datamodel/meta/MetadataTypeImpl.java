/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.meta
 * File:    MetadataTypeImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.meta;

import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * A simple class that represents different {@link MetadataType}s.
 * Use it when comparing if {@link Metadata} is of the same type.
 * 
 * {@link Metadata} should have a typestring of urn:type
 * where urn might be http://example.com and type the Metadatatype
 * 
 * @author aw
 * @version 0.1
 */
class MetadataTypeImpl implements MetadataType {

	private final String mTypestring;
	private final MetaCardinalityType mCardinality;
	
	MetadataTypeImpl(String typestring, MetaCardinalityType card) {
		NullCheck.check(typestring, "typestring is null");
		NullCheck.check(card, "card is null");
		mTypestring = typestring;
		mCardinality = card;
	}

	@Override
	public String getTypeString() {
		return mTypestring;
	}
	
	@Override
	public MetaCardinalityType getCardinality() {
		return mCardinality;
	}

	public boolean equals(Object o) {
		MetadataType ot;
		if (o == null)
			return false;
		
		if (this == o)
			return true;
		
		if (!(o instanceof MetadataType))
			return false;
		
		ot = (MetadataType) o;
		return ot.getTypeString().equals(getTypeString());
	}
}
