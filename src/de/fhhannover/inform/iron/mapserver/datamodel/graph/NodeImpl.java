/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.graph
 * File:    NodeImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.graph;

import java.util.Collection;
import java.util.Map;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.utils.CollectionHelper;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

class NodeImpl extends GraphElementImpl implements Node {
	
	private final Identifier mIdentifier;
	private final Map<Integer, Link> mLinks;
	private final Node mDummy;
	
	NodeImpl(Identifier i) {
		super();
		NullCheck.check(i, "Identifier is null");
		mIdentifier = i;
		mLinks = CollectionHelper.provideMapFor(Integer.class, Link.class);
		mDummy = new DummyNodeImpl(i);
	}

	@Override
	public Identifier getIdentifier() {
		return mIdentifier;
	}

	@Override
	public Collection<Link> getLinks() {
		return CollectionHelper.copy(mLinks.values());
	}

	@Override
	public void addLink(Link l) {
		NullCheck.check(l, "link is null");
		if (mLinks.containsKey(l.hashCode()))
			throw new SystemErrorException("Link " + l + " already on " + this);
		
		mLinks.put(l.hashCode(), l);
	}

	@Override
	public void removeLink(Link l) {
		NullCheck.check(l, "link is null");
		if (!mLinks.containsKey(l.hashCode()))
			throw new SystemErrorException("Link " + l + " not on "  + this);
		
		mLinks.remove(l.hashCode());
	}

	@Override
	public void removeAllLinks() {
		mLinks.clear();
	}

	@Override
	public String provideToStringStart() {
		return "node{" + mIdentifier;
	}

	@Override
	public boolean equalsIdentifiers(GraphElement o) {
		if (o == this)
			return true;
		
		if (!(o instanceof Node))
			return false;
		
		return ((Node)o).getIdentifier().equals(getIdentifier());
	}

	@Override
	public Node dummy() {
		return mDummy;
	}

	@Override
	public int getByteCount() {
		return getIdentifier().getByteCount();
	}
}
