/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.graph
 * File:    DummyNodeImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.graph;

import java.util.Collection;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

class DummyNodeImpl extends DummyGraphElement implements Node {
	
	private final Identifier mIdentifier;
	
	DummyNodeImpl(Identifier ident) {
		NullCheck.check(ident, "ident is null");
		mIdentifier = ident;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.Node#getIdentifier()
	 */
	@Override
	public Identifier getIdentifier() {
		return mIdentifier;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.Node#getLinks()
	 */
	@Override
	public Collection<Link> getLinks() {
		throw new SystemErrorException(sErrorString);
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.Node#addLink(de.fhhannover.inform.iron.mapserver.datamodel.graph.Link)
	 */
	@Override
	public void addLink(Link l) {
		throw new SystemErrorException(sErrorString);
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.Node#removeLink(de.fhhannover.inform.iron.mapserver.datamodel.graph.Link)
	 */
	@Override
	public void removeLink(Link l) {
		throw new SystemErrorException(sErrorString);
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.Node#removeAllLinks()
	 */
	@Override
	public void removeAllLinks() {
		throw new SystemErrorException(sErrorString);
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement#equalsIdentifiers(de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement)
	 */
	@Override
	public boolean equalsIdentifiers(GraphElement o) {
		if (o == this)
			return true;
		
		if (!(o instanceof Node))
			return false;
		
		return ((Node)o).getIdentifier().equals(getIdentifier());
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement#dummy()
	 */
	@Override
	public Node dummy() {
		return this;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.SearchAble#getByteCount()
	 */
	@Override
	public int getByteCount() {
		return getIdentifier().getByteCount();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "dnode{" + getIdentifier() + "}";
	}
}
