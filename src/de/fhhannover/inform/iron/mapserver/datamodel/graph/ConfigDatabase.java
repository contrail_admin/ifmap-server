
package de.fhhannover.inform.iron.mapserver.datamodel.graph;

import java.util.Map;
import java.util.HashMap;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;

/* imports to store in cassandra */
import com.google.common.base.Supplier;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.cassandra.thrift.Cassandra;
import org.apache.thrift.TServiceClient;
import com.netflix.astyanax.Cluster;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.ddl.KeyspaceDefinition;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.exceptions.BadRequestException;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;

class ConfigDatabase {
        private AstyanaxContext<Cluster> mClustCtxt;
	private ColumnFamily<String, String> CF_IDTYPE_TO_IDX;
	private ColumnFamily<String, String> CF_IDENT;
	private ColumnFamily<String, String> CF_META;
	private ColumnFamily<String, String> CF_IDENT_TO_LINKS;
	private static String mKsName = "config_db_ks3"; // Keyspace Name
	private int mIdIdx;

	public ConfigDatabase(){
            /* TODO persistence from api-server currently
	    mClustCtxt = initCassandra();
	    auditCassandra();
	    mIdIdx = 0;
            */
	}

	/**
	 * Initialize Cassandra connection, create keyspace + default CF 
	 * No error if it already exists.
	 */
	private AstyanaxContext<Cluster> initCassandra() {
		AstyanaxContext<Cluster> cContext;

		// Get cluster Context to create keyspace
		cContext = new AstyanaxContext.Builder()
		                .forCluster("Test Cluster")
		                .withAstyanaxConfiguration(
		                   new AstyanaxConfigurationImpl()
		                         .setDiscoveryType(
		                            NodeDiscoveryType.NONE)
		                 )
		                .withConnectionPoolConfiguration(
		                   new ConnectionPoolConfigurationImpl("CdbConnPool")
		                         .setPort(9160) //TODO rm magic
		                         .setMaxConnsPerHost(1) //TODO rm magic
		                         .setSeeds("127.0.0.1:9160") //TODO rm magic
		                 )
		                .withConnectionPoolMonitor(
		                    new CountingConnectionPoolMonitor()
		                 )
		                .buildCluster(
		                    ThriftFamilyFactory.getInstance()
		                 );
		cContext.start();

		return cContext;
	}
	
	/**
	 * Minimum stuff we need in cassandra. Try to create and don't barf if
	 * it exists already.
	 */
	private void auditCassandra() {
		Cluster cluster = mClustCtxt.getEntity();

                // Default CFs that need to exist
		CF_IDTYPE_TO_IDX =
		    new ColumnFamily<String, String>(
		           "idtype_to_idx_dictionary", // CF TODO rm magic
		           StringSerializer.get(),     // Key Serializer
		           StringSerializer.get());    // Column Serializer
	
		CF_IDENT =
		    new ColumnFamily<String, String>(
		           "identifier",             // CF TODO rm magic
		           StringSerializer.get(),   // Key Serializer
		           StringSerializer.get());  // Column Serializer
	
		CF_IDENT_TO_LINKS =
		    new ColumnFamily<String, String>(
		           "identifier_to_links",     // CF TODO rm magic
		           StringSerializer.get(),   // Key Serializer
		           StringSerializer.get());  // Column Serializer
	
		CF_META=
		    new ColumnFamily<String, String>(
		           "metadata",               // CF TODO rm magic
		           StringSerializer.get(),   // Key Serializer
		           StringSerializer.get());  // Column Serializer
		
		try {
		    KeyspaceDefinition ksDef = cluster.makeKeyspaceDefinition();
		    Map<String, String> stratOpt = new HashMap<String, String>();
		    stratOpt.put("replication_factor", "1"); //TODO rm magic
		
		    ksDef.setName(mKsName)
		           .setStrategyClass("SimpleStrategy")
			   .setStrategyOptions(stratOpt)
		           .addColumnFamily(
		             cluster.makeColumnFamilyDefinition()
		                      .setName(CF_IDTYPE_TO_IDX.getName())
		                      .setComparatorType("UTF8Type"))
		           .addColumnFamily(
		             cluster.makeColumnFamilyDefinition()
		                      .setName(CF_IDENT.getName())
		                      .setComparatorType("UTF8Type"))
		           .addColumnFamily(
		             cluster.makeColumnFamilyDefinition()
		                      .setName(CF_META.getName())
		                      .setComparatorType("UTF8Type"))
		           .addColumnFamily(
		             cluster.makeColumnFamilyDefinition()
		                      .setName(CF_IDENT_TO_LINKS.getName())
		                      .setComparatorType("UTF8Type"));

                    // Create keyspace, don't barf if it exists already
		    cluster.addKeyspace(ksDef);
		
		    // TODO is below needed?
		    AstyanaxContext<Keyspace> kContext;
		    kContext = new AstyanaxContext.Builder()
		                    .forCluster("Test Cluster")
		                    .forKeyspace(mKsName)
		                    .withAstyanaxConfiguration(
		                       new AstyanaxConfigurationImpl()
		                             .setDiscoveryType(
		                                NodeDiscoveryType.NONE)
		                     )
		                    .withConnectionPoolConfiguration(
		                       new ConnectionPoolConfigurationImpl("CdbConnPool")
		                             .setPort(9160)
		                             .setMaxConnsPerHost(1)
		                             .setSeeds("127.0.0.1:9160")
		                     )
		                    .withConnectionPoolMonitor(
		                        new CountingConnectionPoolMonitor()
		                     )
		                    .buildKeyspace(
		                        ThriftFamilyFactory.getInstance()
		                     );
		    kContext.start();

		    KeyspaceDefinition ki = kContext.getEntity().describeKeyspace();
		    System.out.println("Describe Keyspace: " + ki.getName());
		} catch (BadRequestException e) {
                    // TODO verify if keyspace exists that CF definitions are proper

		    // TODO only ignore keyspace already exists
		    System.out.println("BadRequestException: " + e.getMessage());
		} catch (Exception e) {
		    System.out.println(e.getMessage());
		}
	}

        public void addToDb(Metadata md, boolean isSingleValue,
	                    Identifier i1, Identifier i2) {

                /* TODO persistence from api-server currently */

		//Keyspace cdbKs = mClustCtxt.getEntity().getKeyspace(mKsName);
		//MutationBatch op = cdbKs.prepareMutationBatch();
		//
		//if (i1 != null) {
		//    op.withRow(CF_IDENT, getIdentifierRowKey(i1))
		//      .putColumn("column1", mIdIdx++, null); // TODO get from ZK
		//}
		//if (i2 != null) {
		//    op.withRow(CF_IDENT, getIdentifierRowKey(i2))
		//      .putColumn("column1", mIdIdx++, null); // TODO get from ZK
		//}

		///*
		// * TODO Change to put values in position-named column 
		// * Also need zk for row locking
		// */
		//String mdRowKey = getMetadataRowKey(md, i1, i2);
    		//String mdPrevVal = "";

		//if (isSingleValue == false) {
		//    try {
		//        Column<String> mdPrev = cdbKs.prepareQuery(CF_META)
		//            			     .getKey(mdRowKey)
		// 			      	     .getColumn("value")
		//			      	     .execute()
		//			      	     .getResult();
    		//        mdPrevVal = mdPrev.getStringValue();
		//    } catch (ConnectionException e) {
		//        System.out.println("Metadata didn't exist before ");
		//    }
		//}

		//try {
		//    op.withRow(CF_META, mdRowKey)
		//      .putColumn("value", mdPrevVal + md.getMetadataAsString(),
		//                 null);
		//    OperationResult<Void> result = op.execute();
		//} catch (ConnectionException e) {
		//    System.out.println("Execute error: " + e.getMessage());
		//}
	}

	private String getIdentifierRowKey(Identifier ident) {
		// TODO replace with dict value for type + idx
		return ident.getValueString();
	}

	private String getMetadataRowKey(Metadata md,
	        			 Identifier i1, Identifier i2) {
		StringBuffer metaRowKey = new StringBuffer();

		// TODO replace with dict value for type + idx
		metaRowKey.append(md.getType().getTypeString());

	        if (i1 != null) {
		    metaRowKey.append("-" + getIdentifierRowKey(i1));
		}
	        if (i2 != null) {
		    metaRowKey.append("-" + getIdentifierRowKey(i2));
		}

		return metaRowKey.toString();
	}
}
