/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.graph
 * File:    Node.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.graph;

import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import java.util.Collection;

/**
 * Interface to access {@link Node} objects in the MAP graph.
 * A {@link Node} has an {@link Identifier} object attached.
 * 
 * {@link Node} objects may have {@link Link} objects, which link them with
 * other {@link Node} objects.
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface Node extends GraphElement {
	
	/**
	 * @return a reference to the {@link Identifier} object attached to this
	 * 			{@link GraphElement} object.
	 */
	public Identifier getIdentifier();
	
	/**
	 * @return a list of all {@link Link} attached to this {@link Node} object.
	 */
	public Collection<Link> getLinks();
	
	/**
	 * Add a {@link Link} object to this {@link Node} object.
	 * 
	 * @param l
	 */
	public void addLink(Link l);
	
	/**
	 * Remove a {@link Link} object from this {@link Node} object.
	 * 
	 * @param l
	 */
	public void removeLink(Link l);
	
	/**
	 * Remova ll {@link Link} objects attached to this {@link Node} object.
	 */
	public void removeAllLinks();

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement#dummy()
	 */
	@Override
	public Node dummy();

}
