/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.graph
 * File:    Link.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.graph;



/**
 * Interface to access {@link Link} objects in the MAP graph.
 * A {@link Link} has to {@link Node} objects attached, representing
 * the ends.
 * 
 * @since 0.3.0
 * @author aw
 */
public interface Link extends GraphElement {
	
	/**
	 * @return a reference to the first {@link Node} object, always non-null.
	 */
	public Node getNode1();
	
	/**
	 * @return a reference to the second {@link Node} object, always non-null.
	 */
	public Node getNode2();

	/**
	 * Give a {@link Node} instance, return the neighbor node attached to this
	 * {@link Link} instance.
	 * 
	 * @param n
	 * @return
	 */
	public Node getNeighborNode(Node n);
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement#dummy()
	 */
	@Override
	public Link dummy();
}
 
