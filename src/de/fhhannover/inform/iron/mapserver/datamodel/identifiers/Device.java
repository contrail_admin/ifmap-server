/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    Device.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidIdentifierException;

/**
 * Implementation of the device Identifier.
 * 
 * @since 0.1.0
 * @author aw 
 */
public class Device extends IdentifierImpl implements Identifier {
	
	private final String mValue;
	private final DeviceTypeEnum mType;
	
	/**
	 * Constructor using type and value value.length has to be greater 0
	 * 
	 * @param value
	 * @param type
	 * @throws InvalidIdentifierException 
	 */
	public Device(final String value, final DeviceTypeEnum type) throws InvalidIdentifierException {
		super(IfmapConstStrings.DEV);
		
		if (value == null || type == null || value.length() == 0)
			throw new InvalidIdentifierException("Device: Invalid value or type"
					+ " (" + value + "|" + type + ")");

		// Use of aik-name is deprecated, do not support it at all.
		if (type == DeviceTypeEnum.aikName)
			throw new InvalidIdentifierException("Device: aik-name is deprecated");
		
		
		mValue = value;
		this.mType = type;
		setByteCount(IfmapConstStrings.DEV_CNT + value.length());
	}
	
	public String getValue() {
		return mValue;
	}
	
	public DeviceTypeEnum getDeviceType() {
		return mType;
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		
		if (this == o)
			return true;
		
		if (!(o instanceof Device))
			return false;
			
		return mValue.equals(((Device)o).getValue());
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierImpl#buildToStringString()
	 */
	@Override
	public String buildToStringString() {
		return "dev{" + getValue() + "}";
	}
}
 
