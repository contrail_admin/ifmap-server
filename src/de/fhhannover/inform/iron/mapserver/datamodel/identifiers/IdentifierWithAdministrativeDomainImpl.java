/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    IdentifierWithAdministrativeDomainImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;

/**
 * @since 0.3.0
 * @author aw
 */
abstract class IdentifierWithAdministrativeDomainImpl extends IdentifierImpl 
		implements IdentifierWithAdministrativeDomain {
	
	private final String administrativeDomain;
	
	/**
	 * The bytes used for the administrativeDomain attribute, if at all
	 */
	private final int mAdmByteCount;
	
	IdentifierWithAdministrativeDomainImpl(final String type, String ad) {
		super(type);
	
		ad = (ad == null) ? "" : ad;
		mAdmByteCount = (ad.length() == 0) ? 0 : IfmapConstStrings.ADMDOM_CNT + ad.length();
		administrativeDomain = ad;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierWithAdministrativeDomain#getAdministrativeDomain()
	 */
	@Override
	public String getAdministrativeDomain() {
		return administrativeDomain;
	}
	
	/**
	 * @return the number of bytes needed to represent the administrativeDomain
	 *         attribute, or zero if none is set.
	 */
	protected final int getByteCountForAdministrativeDomain() {
		return mAdmByteCount;
	}

	@Override
	public boolean equals(Object o) {
		IdentifierWithAdministrativeDomainImpl oident;
		String oad;
		
		if(this == o) 
			return true;
		
		if (o instanceof IdentifierWithAdministrativeDomainImpl) {
			oident = (IdentifierWithAdministrativeDomainImpl) o;
			oad = oident.getAdministrativeDomain();
			
			if (mConf.getAdministrativeDomainIsCaseSensitive())
				return administrativeDomain.equals(oad);
			
			return administrativeDomain.equalsIgnoreCase(oad);
		}
		return false;
	}
}
