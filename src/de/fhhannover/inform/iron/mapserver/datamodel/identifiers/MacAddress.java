/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    MacAddress.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidIdentifierException;
import de.fhhannover.inform.iron.mapserver.utils.MacAddressValidator;

/**
 * Implementation of the mac-address identifier.
 * 
 * @since 0.1.0
 * @author aw
 */
public class MacAddress extends IdentifierWithAdministrativeDomainImpl 
		implements IdentifierWithAdministrativeDomain {
	
	private final String mValue;
	
	/**
	 * Create a MAC Address based on value and administrative-domain
	 * 
	 * @param value
	 * @param ad
	 * @throws InvalidIdentifierException 
	 */
	public MacAddress(final String value, final String ad) throws InvalidIdentifierException {
		super(IfmapConstStrings.MAC, ad);
		
		if (value == null || value.length() == 0)
			throw new InvalidIdentifierException("MacAddress: Empty or no value "
					+ "(" + value + ")");
		
		if (!MacAddressValidator.validateMacAddress(value))
			throw new InvalidIdentifierException("MacAddress: Invalid format " +
					"(" + value + ")");
		mValue = value;
		
		setByteCount(IfmapConstStrings.MAC_CNT + mValue.length()
				+ getByteCountForAdministrativeDomain());
	}
	
	/**
	 * Create a MAC address based only on the value.
	 * The administrativeDomain is set to ""
	 * 
	 * @param value
	 * @throws InvalidIdentifierException 
	 */
	public MacAddress(final String value) throws InvalidIdentifierException {
		this(value, "");
	}
	
	public String getValue() {
		return mValue;
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierWithAdministrativeDomainImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		
		if (this == o)
			return true;
		
		if (!super.equals(o))
			return false;
	
		if (!(o instanceof MacAddress))
			return false;
		
		return mValue.equals(((MacAddress)o).getValue());
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierImpl#buildToStringString()
	 */
	@Override
	public String buildToStringString() {
		String ad = getAdministrativeDomain();
		StringBuffer sb = new StringBuffer();
		sb.append("mac{");
		sb.append(getValue());
		if (!(ad == null | ad.length() == 0)) {
			sb.append(", ");
			sb.append(ad);
		}
		sb.append("}");
		return sb.toString();
	}
}