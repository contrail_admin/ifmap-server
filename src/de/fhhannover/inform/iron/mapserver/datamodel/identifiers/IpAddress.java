/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    IpAddress.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidIdentifierException;
import de.fhhannover.inform.iron.mapserver.utils.IpAddressValidator;

/**
 * Implementation of the ip-address identifier.
 * 
 * @since 0.1.0
 * @author aw
 */
public class IpAddress extends IdentifierWithAdministrativeDomainImpl
		implements IdentifierWithAdministrativeDomain {
	
	private final String mValue;
	private final IpAddressTypeEnum mType;
	
	/**
	 * Construct a IpAddress from value, administrative-domain and type.
	 * 
	 * @param value
	 * @param ad
	 * @param ipat
	 * @throws InvalidIdentifierException
	 */
	public IpAddress(final String value, final String ad, IpAddressTypeEnum ipat)
			throws InvalidIdentifierException {
		super(IfmapConstStrings.IP, ad);
		boolean valid = false;
	
		// default to IPv4
		if (ipat == null)
			ipat = IpAddressTypeEnum.IPv4;
		
		if (value == null || value.length() == 0) 
			throw new InvalidIdentifierException("IpAddress: Empty or no value "
					+ "(" + value + ")");

		mType = ipat;
		mValue = value;
		
		if (ipat == IpAddressTypeEnum.IPv4)
			valid = IpAddressValidator.validateIPv4(value);
		else
			valid = IpAddressValidator.validateIPv6(value);

		if (!valid)
			throw new InvalidIdentifierException("IpAddress: Invalid " +
					mType.toString() + " format (" + value + ")");
		
		setByteCount(IfmapConstStrings.IP_CNT +  mType.toString().length()
				+ mValue.length()  + getByteCountForAdministrativeDomain());
	}
	
	public IpAddress(final String value, final IpAddressTypeEnum ipat)
			throws InvalidIdentifierException {
		this(value, "", ipat);
	}

	public String getValue() {
		return mValue;
	}

	public IpAddressTypeEnum getIpAddressType() {
		return mType;
	}

	@Override
	public boolean equals(Object o) {
		IpAddress oIp;

		if (this == o)
			return true;
		
		if (!super.equals(o))
			return false;
		
		if (!(o instanceof IpAddress))
			return false;
		
		oIp = (IpAddress) o;
		return (mType == oIp.getIpAddressType() && mValue.equals(oIp.getValue()));
	}

	@Override
	public String buildToStringString() {
		String ad = getAdministrativeDomain();
		StringBuffer sb = new StringBuffer();
		sb.append("ip{");
		sb.append(getValue());
		if (!(ad == null | ad.length() == 0)) {
			sb.append(", ");
			sb.append(ad);
		}
		sb.append(", ");
		sb.append(mType.toString());
		sb.append("}");
		return sb.toString();
	}
}
 
