/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    Identifier.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.datamodel.SearchAble;

/**
 * {@link Identifier} is the basic building block in the MAP graph.
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface Identifier extends SearchAble {
	
	/**
	 * @return the type-string of this {@link Identifier} object. This is the
	 * 			name of the identifiers XML element, e.g. ip-address or
	 * 			mac-address
	 */
	public String getTypeString();

	/**
	 * @return the name/value of {@link Identifier} object. This is the
	 * 			value of the identifiers XML element, e.g. 10.1.1.1 or
	 * 			11:22:33:44:55:66
	 */
	public String getValueString();
}

