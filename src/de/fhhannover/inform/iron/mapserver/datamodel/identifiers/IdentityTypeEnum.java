/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    IdentityTypeEnum.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

/**
 * 
 * @author aw
 * 
 * created: Long time ago
 * 
 * changes:
 *  28.01.10 aw, dl - added trustedPlatformModule (FIXME: missing in spec table)
 *  12.02.10 aw - renamed to IdentityTypeEnum
 * 
 *
 */
public enum IdentityTypeEnum {
	aikName { public String toString() { return "aik-name"; } },
	distinguishedName { public String toString() { return "distinguished-name"; } },
	dnsName { public String toString() { return "dns-name"; } },
	emailAddress { public String toString() { return "email-address"; } },
	hipHit { public String toString() { return "hip-hit"; } },
	kerberosPrincipal { public String toString() { return "kerberos-principal"; } },
	userName { public String toString() { return "username"; } },
	sipUri { public String toString() { return "sip-uri"; } },
	telUri { public String toString() { return "tel-uri"; } },
	other { public String toString() { return "other"; } },
}
