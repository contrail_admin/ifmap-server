/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    IdentifierImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.datamodel.DataModelService;
import de.fhhannover.inform.iron.mapserver.datamodel.SimpleSearchAbleImpl;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.provider.DataModelServerConfigurationProvider;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * @since 0.3.0
 * @author aw
 */
abstract class IdentifierImpl extends SimpleSearchAbleImpl implements Identifier {
	
	private final String mIdentifierType;
	
	private String mToStringString;
	private boolean mHashCached;
	private int mHashValue;
	
	/**
	 * Some {@link Identifier} impelmentations need this for {@link #equals(Object)}
	 */
	protected static DataModelServerConfigurationProvider mConf;
	static {
		mConf = DataModelService.getServerConfiguration();
	}
	
	IdentifierImpl(String type) {
		NullCheck.check(type, "identifier type is null");
		mIdentifierType = type;
		mToStringString = null;
		mHashCached = false;
		mHashValue = 0xDEADBEEF;
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier#getTypeString()
	 */
	@Override
	public final String getTypeString() {
		return mIdentifierType;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier#getValueString()
	 */
	@Override
	public final String getValueString() {
		return getValue();
	}

	/**
	 * @return the type-free string to be used for the {@link #toString()} method, expected
	 * 			to be constant, the same for equal identifiers and cache-able.
	 */
	public abstract String getValue();
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public final String toString() {
		loadToStringString();
		return mToStringString;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		calculateHashValue();
		return mHashValue;
	}
	
	/**
	 * @return the string to be used for the {@link #toString()} method, expected
	 * 			to be constant, the same for equal identifiers and cache-able.
	 */
	abstract String buildToStringString();
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public abstract boolean equals(Object o);

	/**
	 * Helper to load the {@link #toString()} string.
	 */
	private void loadToStringString() {
		if (mToStringString == null) {
			mToStringString = buildToStringString();
			if (mToStringString == null)
				throw new SystemErrorException("Could not build string for"
						+ mIdentifierType);
		}
	}

	/**
	 * Helper to calculate the return value of {@link #hashCode()}.
	 */
	private void calculateHashValue() {
		if (!mHashCached) {
			loadToStringString();
			mHashValue = mToStringString.hashCode();
			mHashCached = true;
		}
	}
}
 
