/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    Identity.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidIdentifierException;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.utils.IpAddressValidator;

/**
 * Implementation of the identity identifier.
 * 
 * @since 0.1.0
 * @author aw
 */
public class Identity extends  IdentifierWithAdministrativeDomainImpl
		implements IdentifierWithAdministrativeDomain {
	
	private final String mName;
	private final IdentityTypeEnum mType;
	private final String mOtherTypeDefinition;
	
	/**
	 * Construct a identity object from name, administrative-domain and the type.
	 * 
	 * *Note:* If we come across a Host Identity Tag (hipHit), treat it as IPv6
	 * address.
	 * 
	 * @param name
	 * @param ad
	 * @param it
	 * @throws InvalidIdentifierException 
	 */
	public Identity(final String name, final String ad, final String otherTypeDef, final IdentityTypeEnum it) throws InvalidIdentifierException {
		super(IfmapConstStrings.ID, ad);
		int byteCount = 0;
		
		if (name == null || name.length() == 0)
			throw new InvalidIdentifierException("Identity: name is invalid"
					+ "(" + name + ")");
		mName = name;
		
		if (it == null)
			throw new InvalidIdentifierException("Identity: type is null");

		mType = it;
		
		if (mType == IdentityTypeEnum.other && 
				(otherTypeDef == null || otherTypeDef.length() == 0))
			throw new InvalidIdentifierException("Identity: type-other "
					+ "requires other-type-definition");
		
		mOtherTypeDefinition = (otherTypeDef == null) ? "" : otherTypeDef;
		
		if (mType == IdentityTypeEnum.hipHit && !IpAddressValidator.validateIPv6(name))
				throw new InvalidIdentifierException("Identity: incorrect "
						+ "HIP-HIT incorrect format (" + name + ")");

		byteCount += (IfmapConstStrings.ID_CNT + mName.length()
				+ mType.toString().length()
				+ getByteCountForAdministrativeDomain());
		
		if (mType == IdentityTypeEnum.other) {
			byteCount += IfmapConstStrings.ID_OTHER_TYPE_DEF_ATTR_CNT;
			byteCount += mOtherTypeDefinition.length();
		}
		setByteCount(byteCount);
	}
	
	
	public Identity(String name, IdentityTypeEnum it) throws InvalidIdentifierException {
		this(name, "", null, it);
	}
	
	public IdentityTypeEnum getIdentityType() {
		return mType;
	}
	
	public String getName() {
		return mName;
	}
	
	/**
	 * @return the other-type-definition attribute if type is other, otherwise
	 * 		we die.
	 */
	public String getOtherTypeDefinition() {
		if (mType != IdentityTypeEnum.other)
			throw new SystemErrorException("Identity: Error calling " +
					" getOtherTypeDefinition() on type=" + mType.toString());
		
		return mOtherTypeDefinition;
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierWithAdministrativeDomainImpl#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		boolean caseSensitive = mConf.getIdentityTypeIsCaseSensitive(mType.toString());
		Identity otherIdentity;
		IdentityTypeEnum otherType;
		String otherName;
		String iotherTypeDef;
		
		if (this == o)
			return true;
	
		// check administrative domain in super-class
		if (!super.equals(o))
			return false;
	
		// is it Identity at all?
		if (!(o instanceof Identity))
			return false;
		
		otherIdentity = (Identity)o;
		otherType = otherIdentity.getIdentityType();
		otherName = otherIdentity.getName();
		
		// same type?
		if (mType != otherType)
			return false;
			
		// compare other-type-definitions, if type="other"
		if (mType == IdentityTypeEnum.other) {
			iotherTypeDef = otherIdentity.getOtherTypeDefinition();
			// we check the other type def case sensitive... ?
			// FIXME: What is the specification saying?
			if (!mOtherTypeDefinition.equals(iotherTypeDef))		
				return false;
		}
		
		// finally compare names.
		return (caseSensitive) ? mName.equals(otherName) : mName.equalsIgnoreCase(otherName);
	}

	@Override
	public String getValue() {
		return getName();
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierImpl#buildToStringString()
	 */
	@Override
	public String buildToStringString() {
		String ad = getAdministrativeDomain();
		StringBuffer sb = new StringBuffer();
		sb.append("id{");
		sb.append(mName);
		if (!(ad == null || ad.length() == 0)) {
			sb.append(", ");
			sb.append(ad);
		}
		sb.append(", ");
		sb.append(mType.toString());
		
		if (mType == IdentityTypeEnum.other) {
			sb.append(", ");
			sb.append(getOtherTypeDefinition());
		}
		sb.append("}");
		return sb.toString();
	}
}
