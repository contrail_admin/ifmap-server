/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.identifiers
 * File:    AccessRequest.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.identifiers;

import de.fhhannover.inform.iron.mapserver.IfmapConstStrings;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidIdentifierException;

/**
 * Implementation of the access-request Identifier
 * 
 * @since 0.1.0
 * @author aw
 */
public class AccessRequest extends IdentifierWithAdministrativeDomainImpl
		implements IdentifierWithAdministrativeDomain {
	
	private final String mName;
	
	/**
	 * Create a AccessRequest Object based on a administrative-domain and a name.
	 * 
	 * @param ad
	 * @param name
	 * @throws InvalidIdentifierException 
	 */
	public AccessRequest(final String name, final String ad) throws InvalidIdentifierException {
		super(IfmapConstStrings.AR, ad);
		
		if (name == null || name.length() == 0)
			throw new InvalidIdentifierException("AccessRequest: " +
					"name is invalid (" + name + ")");
		this.mName = name;
		setByteCount(IfmapConstStrings.AR_CNT + name.length() +
				getByteCountForAdministrativeDomain());
	}
	
	/**
	 * Create a AccessRequest only based on a given name.
	 * The administrative-domain will be set to ""
	 * 
	 * @param name
	 * @throws InvalidIdentifierException 
	 */
	public AccessRequest(final String name) throws InvalidIdentifierException {
		this(name, "");
	}
	
	public String getName() {
		return mName;
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierWithAdministrativeDomain#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		
		if (!super.equals(o))
			return false;
		
		if (!(o instanceof AccessRequest))
			return false;
		
		return mName.equals(((AccessRequest)o).getName());
	}

	@Override
	public String getValue() {
		return getName();
	}

	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.datamodel.identifiers.IdentifierImpl#buildToStringString()
	 */
	@Override
	public String buildToStringString() {
		final String ad = getAdministrativeDomain();
		final StringBuffer sb = new StringBuffer();
		sb.append("ar{");
		sb.append(mName);
		if (!(ad == null || ad.length() == 0)) {
			sb.append(", ");
			sb.append(ad);
		}
		sb.append("}");
		return sb.toString();
	}
}
