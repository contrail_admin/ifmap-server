/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    SearchResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import java.util.List;

import de.fhhannover.inform.iron.mapserver.datamodel.SearchAble;
import de.fhhannover.inform.iron.mapserver.messages.SearchResultType;

/**
 * Access a {@link SearchResult} in a read-only manner. Meaning this interface
 * is only useful for marshalling.
 * 
 * @author aw
 */
public interface SearchResult extends SearchAble {
	
	/**
	 * @return a {@link List} of {@link ResultItem} instances contained in this
	 * 			{@link SearchResult}.
	 */
	public abstract List<ResultItem> getResultItems();
	
	/**
	 * Returns the name of the corresponding subscription or null if no name was
	 * given.
	 * 
	 * @return the name of the {@link Subscription} or null if this
	 * 			{@link SearchResult} is not	contained in a {@link PollResult}.
	 */
	public String getName();

	/**
	 * @return the type of this {@link SearchResult}.
	 */
	public SearchResultType getType();

	/**
	 * @return whether any {@link ResultItem} objects can be found in this
	 * 			{@link SearchResult} instance.
	 */
	boolean isEmpty();
	
	/**
	 * @param o
	 * @return
	 */
	boolean sameNameAndType(SearchResult o);
}
