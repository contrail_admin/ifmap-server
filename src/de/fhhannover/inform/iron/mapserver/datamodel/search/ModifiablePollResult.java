/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    ModifiablePollResult.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import de.fhhannover.inform.iron.mapserver.messages.SearchResultType;

/**
 * TODO
 * 
 * @since 0.3.0
 * @author aw
 */
public interface ModifiablePollResult extends PollResult {
	
	public void addErrorResult(String name);
	/**
	 * Add a new {@link SearchResult} to the {@link PollResult}.
	 * If the last {@link SearchResult} is of the same {@link SearchResultType}
	 * and the same {@link Subscription} then the {@link SearchResult} can be
	 * compressed into the last one.
	 * 
	 * @param res
	 */	
	public void addSearchResult(ModifiableSearchResult res);
	
	public void removeResultsOf(String name);
}
