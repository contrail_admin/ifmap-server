/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    SearchHandler.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import de.fhhannover.inform.iron.mapserver.datamodel.graph.Link;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.Node;
import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.exceptions.SearchResultsTooBigException;

/**
 * TODO: Documentation
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public interface SearchHandler {
	
	public static final boolean SEARCH_HANDLER_DEBUG = false;

	/**
	 * @return the {@link Identifier} object to start the search from.
	 */
	public Identifier getStartIdentifier();

	/**
	 * Called before the search is started.
	 */
	public void onStart();

	/**
	 * Called when a {@link Node} is visited.
	 * 
	 * @param cur
	 */
	public void onNode(Node cur) throws SearchResultsTooBigException;

	/**
	 * Called after {@link #onNode(Node)} to indicate whether the {@link Link}
	 * objects of this {@link Node} should be be traveled.
	 * 
	 * @param cur
	 * @return
	 */
	public boolean travelLinksOf(Node cur);

	/**
	 * Called for each {@link Link} object of a {@link Node} object to indicate
	 * whether {@link #onLink(Link)} should be called for this {@link Link}
	 * object.
	 * 
	 * @param l
	 * @return
	 */
	public boolean travelLink(Link l);

	/**
	 * @param neighborNode
	 * @return true if this node should be traversed.
	 */
	public boolean traverseTo(Node nextNode);

	/**
	 * Called directly after {@link #travelLink(Link)} was evaluated to
	 * <code>true</code>. After {@link #onLink(Link)}, the next {@link Node}
	 * of the given {@link Link} object will be traversed to.
	 * 
	 * @param l
	 */
	public void onLink(Link l) throws SearchResultsTooBigException;

	/**
	 * Called after all {@link Link} objects of a {@link Node} object were
	 * visited.
	 * 
	 * @param cur
	 */
	public void afterNode(Node cur);

	/**
	 * Called after the search is finished.
	 */
	public void onEnd();

	public void nextDepth();

	public void depthOver();

}
