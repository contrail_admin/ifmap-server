/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    Filter.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import java.util.Map;

import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * A Filter object simple represents a filter string given by some MAPC.
 * 
 * A Filter string can also be used to match nothing or match everything.
 * 
 * To construct a filter that matches everything no filter string
 * has to be given (e.g. null).
 * 
 * If a filter should match nothing, a empty filter string has to be given.
 * (i.e. a string with length zero).
 * 
 * The above mentioned cases can be asked with isMatchEverything() and
 * isMatchNothing(). Note that !isMatchEverything() does not imply
 * isMatchNothing() (or the other way round) as both can be false.
 * 
 * Besides having a string which represents a filter we need the namespace
 * prefix namespace uri mapping. Having this we can make our Filter
 * namespace aware.
 * This implementation started without such a mapping. Having 
 * no namespace awareness.
 * 
 * TODO:
 * Check whether or not this filter has the correct form
 * 
 * @author aw
 * @version 0.1
 */

public class Filter {
	
	private final String mFilterString;
	private final Map<String, String> mPrefixUriMap;
	
	/**
	 * toStringString cache;
	 */
	private String toStringString;
	
	/**
	 * Construct a filter from a given filter string
	 * and a map of namespace prefixes with namespace uris.
	 * 
	 * If no map is given a map with only the meta namespace
	 * mapping is used
	 * 
	 * 
	 * @param fs
	 * @param hu
	 */
	public Filter(String fs, Map<String, String> hu) {
		NullCheck.check(hu, "No Namespace Mapping given");
		mPrefixUriMap = hu;
		mFilterString = fs;
		toStringString = null;
	}
	
	public String getFilterString() {
		if (isMatchEverything() || isMatchNothing())
			throw new SystemErrorException("get filter for match all/nothing");
		
		return mFilterString;
	}

	public boolean isMatchNothing() {
		return mFilterString != null && mFilterString.length() == 0;
	}


	public boolean isMatchEverything() {
		return mFilterString == null;
	}
	
	/**
	 * Returns a hashmap with all namespace prefix namespace
	 * uri mappgings known to this filter.
	 * 
	 * @return
	 */
	public Map<String, String> getNamespaceMap() {
		return mPrefixUriMap;
	}
	
	public String toString() {
		if (toStringString == null)
			toStringString = buildToStringString();
		return toStringString;
	}

	private String buildToStringString() {
		StringBuffer sb = new StringBuffer("filter{");
		
		if (isMatchEverything())
			sb.append("match everything");
		else if (isMatchNothing())
			sb.append("match nothing");
		else
			sb.append(mFilterString);
		
		sb.append("}");
		return sb.toString();
	}
}
