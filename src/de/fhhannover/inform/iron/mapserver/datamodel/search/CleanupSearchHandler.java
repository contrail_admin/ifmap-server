/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    CleanupSearchHandler.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import org.apache.log4j.Logger;

import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.Link;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.Node;
import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.exceptions.SearchResultsTooBigException;
import de.fhhannover.inform.iron.mapserver.provider.LoggingProvider;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

class CleanupSearchHandler implements SearchHandler {
	
	private static final Logger sLogger = LoggingProvider.getTheLogger();
	private static final String sName = "CleanupSearchHandler";
	
	private final Identifier mStartIdent;
	private final Subscription mSubscription;
	private final String mName;
	
	CleanupSearchHandler(Identifier start, Subscription sub) {
		NullCheck.check(start, "start identifier is null");
		NullCheck.check(sub, "sub is null");
		mStartIdent = start;
		mSubscription = sub;
		mName = sName + "[" + sub.getName() + "]";
	}

	@Override
	public Identifier getStartIdentifier() {
		return mStartIdent;
	}

	@Override
	public void onStart() {
		if (SearchHandler.SEARCH_HANDLER_DEBUG)
			sLogger.trace(mName +": Start for " + mSubscription + " at "
					+ getStartIdentifier());
	}

	@Override
	public void onNode(Node cur) throws SearchResultsTooBigException {
		cleanGraphElement(cur);
	}

	@Override
	public boolean travelLinksOf(Node cur) {
		return true;
	}

	@Override
	public boolean travelLink(Link l) {
		return (l.getRemovedSubscriptionEntry(mSubscription) != null);
	}

	@Override
	public boolean traverseTo(Node nextNode) {
		return nextNode.getRemovedSubscriptionEntry(mSubscription) != null;
	}

	@Override
	public void onLink(Link l) throws SearchResultsTooBigException {
		cleanGraphElement(l);
	}


	@Override
	public void afterNode(Node cur) {
		// nothing
	}

	@Override
	public void onEnd() {
		if (SearchHandler.SEARCH_HANDLER_DEBUG)
			sLogger.trace(mName + ": Finished " + mSubscription);
	}

	@Override
	public void nextDepth() {
		// nothing
	}

	@Override
	public void depthOver() {
		// nothing
	}

	private void cleanGraphElement(GraphElement ge) {
		SubscriptionEntry entry = ge.getRemovedSubscriptionEntry(mSubscription);
		
		if (SearchHandler.SEARCH_HANDLER_DEBUG)
			sLogger.trace(mName + ": Cleaning " + ge);
		
		if (entry == null) {
			if (SearchHandler.SEARCH_HANDLER_DEBUG)
				sLogger.trace(mName + ": No entry here anymore");
			return;
		}
		
	
		if (SearchHandler.SEARCH_HANDLER_DEBUG)
			sLogger.trace(mName + ": Removing old sub entry from " + ge + " for "
					+ mSubscription);
		
		ge.removeRemovedSubscriptionEntry(mSubscription);
	}
}
