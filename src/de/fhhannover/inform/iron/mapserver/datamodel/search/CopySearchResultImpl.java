/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    CopySearchResultImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;
import de.fhhannover.inform.iron.mapserver.messages.SearchResultType;
import de.fhhannover.inform.iron.mapserver.utils.NullCheck;

/**
 * A simple subclass of {@link SearchResult}.
 * 
 * <b>This class does not overwrite the {@link #getByteCount()} method!</b>
 * 
 * @version 0.1
 * @author aw
 * 
 */
class CopySearchResultImpl extends SearchResultImpl {
	
	/**
	 * Constructs a {@link CopySearchResultImpl} object with a name and type.
	 */
	CopySearchResultImpl(String name, SearchResultType type) {
		super(name, type);
	}

	/**
	 * Constructs a {@link CopySearchResultImpl} object with a name.
	 */
	CopySearchResultImpl(String name) {
		this(name, SearchResultType.SEARCH);
	}
	
	/**
	 * Constructs a {@link CopySearchResultImpl} object without a name.
	 */
	CopySearchResultImpl() {
		this(null);
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.messages.SearchResult#addMetadata(de.fhhannover.inform.iron.mapserver.datamodel.graph.MetadataContainer, de.fhhannover.inform.iron.mapserver.datamodel.graph.meta.Metadata)
	 */
	@Override
	public void addMetadata(GraphElement ge, Metadata m) {
		
		NullCheck.check(ge, "addMetadata() with no GraphElement");
	
		// If mc is not the same as used in the last ResultItem, we have
		// to put a new one in the list.
		if (mLastResultItem == null || !ge.equalsIdentifiers(mLastGraphElement)) {
			mLastGraphElement = ge.dummy();
			mLastResultItem = new ResultItem(mLastGraphElement);
			mResultItems.add(mLastResultItem);
		}
		
		if (m != null)
			mLastResultItem.addMetadata(m);
	}
}
