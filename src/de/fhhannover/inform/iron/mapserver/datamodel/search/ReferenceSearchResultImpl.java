/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel.search
 * File:    ReferenceSearchResultImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel.search;

import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;

/**
 * {@link SearchResult} implementation which does not copy the added
 * {@link GraphElement} and {@link Metadata} objects.
 * 
 * 
 * As a result, a {@link SearchResult} created with this implementation will
 * contain references to objects in the real graph for by-reference comparison.
 * <br/><b>Be careful!</b>
 * 
 * @version 0.1
 * @author aw, vp
 */
public class ReferenceSearchResultImpl extends SearchResultImpl {

	ReferenceSearchResultImpl(String name) {
		super(name);
		
		if (name != null)
			throw new SystemErrorException("this should never contain a name");
	}
	
	ReferenceSearchResultImpl() {
		this(null);
	}
	
	/* (non-Javadoc)
	 * @see de.fhhannover.inform.iron.mapserver.messages.SearchResult#addMetadata(de.fhhannover.inform.iron.mapserver.datamodel.graph.MetadataContainer, de.fhhannover.inform.iron.mapserver.datamodel.graph.meta.Metadata)
	 */
	public void addMetadata(GraphElement ge, Metadata m) {
		
		if (ge == null)
			throw new SystemErrorException("addMetadata() with no MetadataContainer");
	
		// If mc is not the same as used in the last ResultItem, we have
		// to put a new one in the list.
		if (mLastResultItem == null || ge != mLastResultItem.getGraphElement()) {
			mLastResultItem = new ResultItem(ge);
			mResultItems.add(mLastResultItem);
		}
		
		if (m != null)
			mLastResultItem.addMetadata(m);
	}
}

