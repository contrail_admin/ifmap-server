/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel
 * File:    PublishService.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel;

import java.util.List;

import org.apache.log4j.Logger;

import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElement;
import de.fhhannover.inform.iron.mapserver.datamodel.graph.GraphElementRepository;
import de.fhhannover.inform.iron.mapserver.datamodel.identifiers.Identifier;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.Metadata;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.MetadataHolder;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.MetadataHolderFactory;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.MetadataLifeTime;
import de.fhhannover.inform.iron.mapserver.datamodel.meta.MetadataState;
import de.fhhannover.inform.iron.mapserver.datamodel.search.Filter;
import de.fhhannover.inform.iron.mapserver.exceptions.InvalidMetadataException;
import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;
import de.fhhannover.inform.iron.mapserver.messages.PublishDelete;
import de.fhhannover.inform.iron.mapserver.messages.PublishNotify;
import de.fhhannover.inform.iron.mapserver.messages.PublishRequest;
import de.fhhannover.inform.iron.mapserver.messages.PublishUpdate;
import de.fhhannover.inform.iron.mapserver.messages.SubPublishRequest;
import de.fhhannover.inform.iron.mapserver.provider.LoggingProvider;
import de.fhhannover.inform.iron.mapserver.utils.CollectionHelper;
import de.fhhannover.inform.iron.mapserver.utils.Iso8601DateTime;

/**
 * Publish or delete metadata.
 *
 * @author awe, vp
 * @since 0.1.0
 */
class PublishService {
	
	/**
	 * a static logger instance
	 */
	private final static Logger sLogger = LoggingProvider.getTheLogger();
	private final static String sName = "PublishService";
	
	private PublisherRep mPublisherRep;
	private GraphElementRepository mGraph;
	private SubscriptionService mSubService;
	private MetadataHolderFactory mMetaHolderFac;
	
	PublishService(PublisherRep pRep, GraphElementRepository graphRep,
			MetadataHolderFactory metaHolderFac, SubscriptionService subServ) {
		mPublisherRep = pRep;
		mGraph = graphRep;
		mMetaHolderFac = metaHolderFac;
		mSubService	= subServ;
	}
	
	/**
	 * Go through the whole PublishRequest and dispatch the elements to
	 * the corresponding process* methods
	 *
	 * @param req
	 * @throws InvalidMetadataException 
	 */
	void publish(PublishRequest req) throws InvalidMetadataException {
		List<SubPublishRequest> list = req.getSubPublishRequestList();
		String sId = req.getSessionId();
		String pId = null;
		sLogger.trace(sName + ": processing publishRequest for sessionid " + sId);
		
		Publisher publisher = mPublisherRep.getPublisherBySessionId(sId);
		pId = publisher.getPublisherId();
		
		sLogger.trace(sName + ": " + publisher + " with " + list.size() + " requests");
	
		List<MetadataHolder> changes = CollectionHelper.provideListFor(MetadataHolder.class);
		
		for (SubPublishRequest sreq : list) {
			if (sreq instanceof PublishNotify)
				processPublishNotify(publisher, (PublishNotify) sreq, changes);
			else if (sreq instanceof PublishUpdate)
				processPublishUpdate(publisher, (PublishUpdate)sreq, changes);
			else if (sreq instanceof PublishDelete)
				processPublishDelete(publisher, (PublishDelete)sreq, changes);
			else
				throw new SystemErrorException("Unknown SubPublishRequest implementation");
		}
		
		
		// set timestamp and publisher id _now_ and set references to 
		// GraphElements and Publishers...
		String timeNow = Iso8601DateTime.getTimeNow();
		for (MetadataHolder mh : changes) {
			Metadata m = mh.getMetadata();
			if (mh.isNew() || mh.isNotify()) {
				addOperationalAttributes(m, timeNow, pId);
			}
		}
	
		// FIXME!!
		//mdentifierRep.setTimestamp(System.currentTimeMillis());
		mSubService.commitChanges(changes);
	}
			
	/**
	 * Publish persistent {@link Metadata}.
	 */
	private void processPublishUpdate(Publisher pub, PublishUpdate req,
			List<MetadataHolder> changes) {
		processPublish(pub, req, MetadataState.NEW, changes);
	}

	/**
	 * Publish notify {@link Metadata}.
	 */
	private void processPublishNotify(Publisher pub, PublishNotify req,
			List<MetadataHolder> changes) {
		processPublish(pub, req, MetadataState.NOTIFY, changes);
	}
	
	/**
	 * Delete {@link Metadata} objects.
	 * 
	 * @param pub the publisher responsible for this request
	 * @param req the delete request
	 * @param changes 
	 * @return
	 */
	private void processPublishDelete(Publisher pub, PublishDelete req,
			List<MetadataHolder> changes) {
	
		Identifier i1 = req.getIdent1();
		Identifier i2 = req.getIdent2();
		Filter filter = req.getFilter();
		GraphElement ge = mGraph.getGraphElement(i1, i2);
		List<MetadataHolder> toRemove;
		
		toRemove = ge.getMetadataHolder(filter);
		sLogger.trace(sName + ": publish delete for " + ge + " and "
				+ toRemove.size() + " metadata objects");
		
		for (MetadataHolder mh : toRemove) {
			Metadata m = mh.getMetadata();
			sLogger.trace(sName + ": deleting " + m.getPrefixAndElement() + " from "
					+ ge + " (was state " + mh.getState() + ")");
		
			if (mh.getState() == MetadataState.NEW) {
				removeReferences(mh);
				// silently drop new Metadata objects,
				// sanity check as mh should be in changes
				if (!changes.remove(mh)) {
					sLogger.error(sName + ": deleted NEW metadata not in changes ("
							+ m.getPrefixAndElement() + " on " + ge + ")");
					throw new SystemErrorException("NEW metadata not in changes");
				}
			} else if (mh.getState() == MetadataState.UNCHANGED) {
				// UNCHANGED metadata becomes DELETED, references are removed
				// in SubscriptionService
				mh.setState(MetadataState.DELETED);
				changes.add(mh);
			} else {
				// DELETED or NOTIFY Metadata, we do not need to consider them.
			}
		}
	}
	
	private void addOperationalAttributes(Metadata m, String timeNow, String pId) {
		m.setTimestamp(timeNow);
		m.setPublisherId(pId);
	}

	private void setReferences(MetadataHolder mh) {
		mh.getGraphElement().addMetadataHolder(mh);
		mh.getPublisher().addMetadataHolder(mh);
	}
	
	private void removeReferences(MetadataHolder mh) {
		mh.getGraphElement().removeMetadataHolder(mh);
		mh.getPublisher().removeMetadataHolder(mh);
		
	}

	/**
	 * Helper used by {@link #processPublishUpdate(Publisher, PublishUpdate, List)}
	 * and {@link #processPublishNotify(Publisher, PublishNotify, List)}.
	 * 
	 * @param pub
	 * @param req
	 * @param state
	 * @param changes
	 */
	private void processPublish(Publisher pub, PublishUpdate req, MetadataState state,
			List<MetadataHolder> changes) {
		List<Metadata> mlist = req.getMetadataList();
		List<MetadataHolder> toRemove = null;
		Identifier i1 = req.getIdent1();
		Identifier i2 = req.getIdent2();
		MetadataLifeTime lt = req.getLifeTime();
		GraphElement graphElement = mGraph.getGraphElement(i1, i2);
		MetadataHolder mh = null;
		
		for (Metadata m : mlist) {
			
			// singleValue metadata objects need to be replaced
			if (m.isSingleValue()) {
				toRemove = graphElement.getMetadataHolder(m.getType());
				
				if (toRemove.size() > 1)
					throw new SystemErrorException("singleValue occures more than once");
				
				if (toRemove.size() == 1) {
					MetadataHolder remove = toRemove.get(0);
					
					if (remove.isNew())
						if (!changes.remove(remove))
							throw new SystemErrorException("NEW metadata not in changes");
					
					removeReferences(remove);
				}
			}
			
			mGraph.addToDb(m, m.isSingleValue(), i1, i2);

			// a new MetadataHolder instance for this Metadata instance
			mh = mMetaHolderFac.newMetadataHolder(m, lt, graphElement, pub);
			changes.add(mh);
			mh.setState(state);
			setReferences(mh);
		}
	}
}
 

