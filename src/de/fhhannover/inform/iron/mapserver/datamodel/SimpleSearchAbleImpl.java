/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.datamodel
 * File:    SimpleSearchAbleImpl.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.datamodel;

import de.fhhannover.inform.iron.mapserver.exceptions.SystemErrorException;

public abstract class SimpleSearchAbleImpl implements SearchAble {
	
	/**
	 * indicates whether the byte count was set for this object.
	 */
	private boolean mByteCountSet;
	
	/**
	 * the byte count for this object.
	 */
	private int mByteCount;
	
	/**
	 * construct uninitialized {@link SimpleSearchAbleImpl} object.
	 */
	public SimpleSearchAbleImpl() {
		mByteCountSet = false;
		mByteCount = -1;
	}
	
	/**
	 * construct initialized {@link SimpleSearchAbleImpl} object.
	 * 
	 * @param bytes
	 */
	public SimpleSearchAbleImpl(int bytes) {
		this();
		setByteCount(bytes);
	}
	
	/**
	 * Set the byte count for this {@link SearchAble} object.
	 * @throws SystemErrorException if bytes <= 0 or this method was called
	 * 			before.
	 * 
	 * @param bytes
	 */
	protected final void setByteCount(int bytes) {
		
		if (mByteCountSet)
			throw new SystemErrorException("reinitialization of byte count");
		
		if (bytes <= 0)
			throw new SystemErrorException("byte count " + bytes + " invalid");
		
		mByteCountSet = true;
		mByteCount = bytes;
	}
	
	public final int getByteCount() {
		
		if (!mByteCountSet)
			throw new SystemErrorException("access to uninitialized byte count");
		
		return mByteCount;
	}
}
