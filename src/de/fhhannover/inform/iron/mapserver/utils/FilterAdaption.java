/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    FilterAdaption.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

/**
 * Implements adaptFilterString().
 * 
 * This method transforms a given filter string such that in can be used with
 * standard XPath implementations. Basically, this is for now
 * transforming a "[" to "*[" and " [" to " *[".
 * 
 * @author aw
 *
 */
public class FilterAdaption {
	
	/**
	 * Replace all occurrences of [ without a preceding element or at the
	 * beginning of the string with *[. This is because the * is not used
	 * as wild card in IF-MAP filterstrings.
	 * 
	 * @param fs
	 * @return adapted filterstring
	 */
	public static String adaptFilterString(String fs) {
		NullCheck.check(fs, "filter string is null");
		String first = fs.replaceAll("^\\[", "*[");
		return first.replace(" [", " *[");
	}
}
