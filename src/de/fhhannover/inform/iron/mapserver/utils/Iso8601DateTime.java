/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    Iso8601DateTime.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @since 0.3.0
 */
public class Iso8601DateTime {
	
	/**
	 * Thanks Ingo!
	 * @param dt
	 * @return
	 */
	public static String getTimeNow(){
		Date dt = new Date();
		//FIXME hack in order to get good timestamps for xsd:dateTime
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat tzFormatter = new SimpleDateFormat("Z");
        String timezone = tzFormatter.format(dt);
        return formatter.format(dt) + timezone.substring(0, 3) + ":"
                + timezone.substring(3);
	}
}
