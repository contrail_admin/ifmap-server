/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    MultiMap.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

import java.util.Collection;
import java.util.Set;

/**
 * A map which allows multiple entries of the same key.
 * 
 * @author aw
 *
 * @param <K>
 * @param <V>
 */
public interface MultiMap<K, V> {
	
	public V get(K key, V value);
	
	public boolean remove(K key, V value);

	public Collection<V> getAll(K key);
	
	public boolean removeAll(K key);

	int size();

	boolean isEmpty();

	boolean containsKey(Object key);

	V put(K key, V value);

	void clear();

	Set<K> keySet();

	Collection<V> values();
}
