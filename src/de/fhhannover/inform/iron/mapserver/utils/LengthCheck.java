/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    LengthCheck.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

public class LengthCheck {

	public static void checkMin(String test, int min, String msg) {
		NullCheck.check(test, "checkMin() bad argument");
		if (test.length() < min) {
			throw new RuntimeException("checkMin() " + msg + " (length=" +
					test.length() + " min=" + min + ")");
		}
	}

	public static void checkMax(String test, int max, String msg) {
		NullCheck.check(test, "checkMax() bad argument");
		if (test.length() > max) {
			throw new RuntimeException("checkMax() " + msg + " (length=" +
					test.length() + " max=" + max + ")");
		}
	}

	public static void checkMinMax(String test, int min, int max, String msg) {
		checkMin(test, min, msg);
		checkMax(test, max, msg);
	}
}
