/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    IpAddressValidator.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

/**
 * Class which contains to check methods
 * for validating IP Adresses.
 * 
 * @author aw
 * @version 0.1
 * 
 * created: 26.11.09
 * changes:
 *  26.11.09 aw - Implemented first version for IPv4 and IPv6
 *  27.11.09 aw - Check for null values
 *
 */
public class IpAddressValidator {
	
	/**
	 * Check a IPv4 Address with splitting at the dots.
	 * 
	 * FIXME: Accepts 127.0.0.1.... (... means following dots!)
	 * 
	 * @param value
	 * @return
	 */
	public static boolean validateIPv4(String value) {
		boolean res = true;
		
		if (value == null)
			return false;
		
		int dots = 0;
		for (byte b : value.getBytes()) {
			if (b == '.') dots++;
			if (dots > 3) {
				res = false;
			}
		}
			
		try {
			String octets[] = value.split("\\.");
			if (octets.length == 4 && res) {
				for (String s : octets) {
					if (s.length() == 0 || (s.startsWith("0") && s.length() > 1)) {
						res = false;
						break;
					}
					int x = Integer.parseInt(s);
					if (x > 255 || x < 0) {
						res = false;
						break;
					}
				}
			} else {
				res = false;
			}
		} catch (NumberFormatException e) {
			res = false;
		}
		return res;
	}
	
	/**
	 * Validate IPv6 Addresses those must be lowercase
	 * hexdigits and at least one zero is needed.
	 * @param value
	 * @return
	 */
	public static boolean validateIPv6(String value) {
		boolean res = true;
		
		if (value == null)
			return false;
		
		try {
			String quads[] = value.split(":");
			if (quads.length == 8) {
				for (String s : quads) {
					if (s.length() == 0 || (s.startsWith("0") && s.length() > 1)) {
						res = false;
						break;
					}
					
					int numb = Integer.parseInt(s, 16);
					if (numb > 0xffff || numb < 0 || !s.toLowerCase().equals(s)) {
						res = false;
						break;
					}
				}
			} else {
				res = false;
			}
		} catch (NumberFormatException e) {
			res = false;
		}
		return res;
	}
}
