/*
 * Project: irond
 * Package: src.de.fhhannover.inform.iron.mapserver.utils
 * File:    CollectionHelper.java
 *
 * Copyright (C) 2010-2011 Fachhochschule Hannover
 * Ricklinger Stadtweg 118, 30459 Hannover, Germany 
 *
 * Licensed under the Apache License, Version 2.0 (the License);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.fhhannover.inform.iron.mapserver.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Lets make use of this to allocate the same type of list, collection and
 * so on all the time...
 * 
 * @since 0.3.0
 * @author aw
 *
 */
public class CollectionHelper {
	
	public static <T> List<T> provideListFor(Class<T> c) {
		return new ArrayList<T>();
	}

	public static <T> List<T> copy(Collection<T> collection) {
		@SuppressWarnings("unchecked")
		List<T>	ret = (List<T>) provideListFor(Object.class);
		ret.addAll(collection);
		return ret;
	}

	public static <T> Collection<T> provideCollectionFor(Class<T> c) {
		return provideListFor(c);
	}

	public static <T, V> Map<T, V> provideMapFor(Class<T> c1, Class<V> c2) {
		return new HashMap<T, V>();
	}

	public static <T> Set<T> provideSetFor(Class<T> c) {
		return new HashSet<T>();
	}
}


